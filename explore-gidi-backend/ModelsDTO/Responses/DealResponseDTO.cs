﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class DealResponseDTO : DealSummaryDTO
    {
        public string Description { get; set; }
        public string DiscountPrice { get; set; }
        public string ValuePrice { get; set; }
        public bool IsFavorite { get; set; }
        public CategoryResponseDTO Category { get; set; }
    }
}
