﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class DealSummaryDTO
    {
        public string Slug { get; set; }
        public string Name { get; set; }
        public string PercentOff { get; set; }
        public string ImageUrl { get; set; }
        public string TimeCreated { get; set; }
        public string ExpiryDate { get; set; }
        public bool IsExpired { get; set; }
        public RestaurantResponseDTO Restaurant { get; set; }
    }
}
