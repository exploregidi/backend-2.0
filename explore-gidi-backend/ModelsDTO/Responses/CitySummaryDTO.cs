﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class CitySummaryDTO
    {
        public string Slug { get; set; }
        public string Name { get; set; }
    }
}
