﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class UserProfileCompleteDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string ProfilePictureUrl { get; set; }
    }
}
