﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class CategoryResponseDTO
    {
        public string Slug { get; set; }
        public string Name { get; set; }
    }
}
