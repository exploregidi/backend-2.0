﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class PartnerProfileDTO : UserProfileCompleteDTO
    {
        public RestaurantSummaryDTO Restaurant { get; set; }
    }
}
