﻿using System;
using System.Collections.Generic;

namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class LoginResponseDTO
    {
        public Guid Id { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public string ExpiryTime { get; set; }
        public ICollection<string> Roles { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }
}
