﻿using System.Collections.Generic;

namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class CityResponseDTO : CitySummaryDTO
    {
        public IEnumerable<DealSummaryDTO> Deals { get; set; }
    }
}
