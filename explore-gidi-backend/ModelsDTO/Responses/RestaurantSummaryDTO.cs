﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class RestaurantSummaryDTO
    {
        public string Slug { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}