﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class RestaurantResponseDTO : RestaurantSummaryDTO
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string SubAccountCode { get; set; }
        public CitySummaryDTO City { get; set; }
    }
}
