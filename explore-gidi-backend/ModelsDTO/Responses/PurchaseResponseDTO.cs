﻿namespace explore_gidi_backend.ModelsDTO.Responses
{
    public class PurchaseResponseDTO
    {
        public string PurchaseCode { get; set; }
        public bool IsRedeemed { get; set; }
        public virtual DealSummaryDTO Deal { get; set; }
        public virtual UserProfileCompleteDTO User { get; set; }
    }
}
