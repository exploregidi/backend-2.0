﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class RestaurantRequestDTO
    {
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 2, ErrorMessage = "{0} must be atleast 2 characters long.")]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "{0} can only contain alphabets")]
        public string Name { get; set; }
        [Url]
        [Required]
        public string ImageUrl { get; set; }
        [Required]
        public string CitySlug { get; set; }
    }
}
