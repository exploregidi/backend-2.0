﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class ResendConfirmEmailTokenDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
