﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class DealUpdateDTO
    {
        /// <summary>
        /// Format: dd/MM/yyyy
        /// </summary>
        [Required]
        public string ExpiryDate { get; set; }
    }
}
