﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class RefreshTokenDTO
    {
        [Required]
        public string RefreshToken { get; set; }
        [Required]
        public string UserId { get; set; }
    }
}
