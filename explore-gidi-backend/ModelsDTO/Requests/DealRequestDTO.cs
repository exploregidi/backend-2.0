﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class DealRequestDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double DiscountPrice { get; set; }
        [Required]
        public double ValuePrice { get; set; }
        /// <summary>
        /// Format: dd/MM/yyyy
        /// </summary>
        [Required]
        public string ExpiryDate { get; set; }
        [Required]
        public string ImageUrl { get; set; }
        public string CategorySlug { get; set; }
    }
}
