﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class ConfirmEmailDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Token { get; set; }
    }
}
