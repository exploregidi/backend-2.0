﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class UserRegisterDTO
    {
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 2, ErrorMessage = "{0} must be atleast 2 characters long.")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "{0} can only contain alphabets")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(int.MaxValue, MinimumLength = 2, ErrorMessage = "{0} must be atleast 2 characters long.")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "{0} can only contain alphabets")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Phone]
        public string PhoneNumber { get; set; }

        [Url]
        public string ProfilePictureUrl { get; set; }

        public bool IsAPartner { get; set; } = false;
    }
}
