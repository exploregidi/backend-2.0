﻿using System;
using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class PurchaseRequestDTO
    {
        [Required]
        public string ReferenceNumber { get; set; }
        [Required]
        public string DealSlug { get; set; }
    }
}
