﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class ChangeProfilePictureDTO
    {
        [Url]
        [Required]
        public string ProfilePictureUrl { get; set; }
    }
}
