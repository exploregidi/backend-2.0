﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class RestaurantSubAccountCodeRequestDTO
    {
        [Required]
        public string SubAccountCode { get; set; }
    }
}
