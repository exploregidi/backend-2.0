﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class FavoriteDealRequestDTO
    {
        [Required]
        public string DealSlug { get; set; }
    }
}