﻿using System.ComponentModel.DataAnnotations;

namespace explore_gidi_backend.ModelsDTO.Requests
{
    public class RedeemPurchaseCodeRequestDTO
    {
        [Required]
        public bool IsRedeemed { get; set; } = true;
    }
}
