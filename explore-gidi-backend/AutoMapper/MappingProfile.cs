﻿using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Utilities;
using System;
using System.Globalization;
using System.Linq;

namespace explore_gidi_backend.AutoMapper
{
    /// <summary>
    /// AutoMapper profile class for mapping Objects to Data Transfer Objects
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// public constructor
        /// </summary>
        public MappingProfile()
        {
            // Accounts Controller
            CreateMap<ApplicationUser, LoginResponseDTO>();
            CreateMap<ApplicationUser, RefreshToken>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(
                    src => src.Id))
                .ForMember(dest => dest.GeneratedTime, opt => opt.MapFrom(
                    src => DateTime.UtcNow))
                .ForMember(dest => dest.ExpiryTime, opt => opt.MapFrom(
                    src => DateTime.UtcNow.AddDays(30)));

            CreateMap<UserRegisterDTO, ApplicationUser>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(
                    src => src.Email))
                .ForAllMembers(opts => opts.Condition(
                    (src, dest, srcMember) => srcMember != null));

            // Cities Controller
            CreateMap<City, CitySummaryDTO>();

            CreateMap<City, CityResponseDTO>()
                .ForMember(dest => dest.Deals, opt => opt.MapFrom(
                    src => src.Restaurants.SelectMany(x => x.Deals)
                                          .OrderByDescending(x => x.TimeCreated)
                                          .Take(AppConstants.TEN)));

            // Deals Controller
            CreateMap<Category, CategoryResponseDTO>();

            CreateMap<Category, Deal>()
                .ForMember(dest => dest.CategoryId, opts => opts.MapFrom(
                    src => src.CategoryId))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<Deal, DealResponseDTO>()
                .ForMember(dest => dest.DiscountPrice, opt => opt.MapFrom(
                    src => string.Format("{0:n}", src.DiscountPrice)))
                .ForMember(dest => dest.ExpiryDate, opt => opt.MapFrom(
                    src => src.ExpiryDate.ToString("MMMM dd, yyyy")))
                .ForMember(dest => dest.TimeCreated, opt => opt.MapFrom(
                    src => src.TimeCreated.ToString("MMMM dd, yyyy")))
                .ForMember(dest => dest.PercentOff, opt => opt.MapFrom(
                    src => src.PercentOff.ToString("0.##")))
                .ForMember(dest => dest.ValuePrice, opt => opt.MapFrom(
                    src => string.Format("{0:n}", src.ValuePrice)))
                .ForMember(dest => dest.IsExpired, opt => opt.MapFrom(
                    src => src.ExpiryDate < DateTime.Now));

            CreateMap<Deal, DealSummaryDTO>()
                .ForMember(dest => dest.PercentOff, opt => opt.MapFrom(
                    src => src.PercentOff.ToString("0.##")))
                .ForMember(dest => dest.ExpiryDate, opt => opt.MapFrom(
                    src => src.ExpiryDate.ToString("MMMM dd, yyyy")))
                .ForMember(dest => dest.TimeCreated, opt => opt.MapFrom(
                    src => src.TimeCreated.ToString("MMMM dd, yyyy")))
                .ForMember(dest => dest.IsExpired, opt => opt.MapFrom(
                    src => src.ExpiryDate < DateTime.Now));

            CreateMap<DealRequestDTO, Deal>()
                .ForMember(dest => dest.PercentOff, opt => opt.MapFrom(
                    src => (src.ValuePrice - src.DiscountPrice) / src.ValuePrice * AppConstants.ONE_HUNDRED))
                .ForMember(dest => dest.ExpiryDate, opt => opt.MapFrom(
                    src => DateTime.ParseExact(src.ExpiryDate, 
                                               "d/M/yyyy", 
                                               CultureInfo.InvariantCulture)));

            CreateMap<DealUpdateDTO, Deal>()
                .ForMember(dest => dest.ExpiryDate, opt => opt.MapFrom(
                    src => DateTime.ParseExact(src.ExpiryDate,
                                               "d/M/yyyy",
                                               CultureInfo.InvariantCulture)))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<FavoriteDeal, DealResponseDTO>()
                .ForMember(dest => dest.IsFavorite, opt => opt.MapFrom(
                    src => src != null ? true : false))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<Restaurant, Deal>()
                .ForMember(dest => dest.RestaurantId, opts => opts.MapFrom(
                    src => src.RestaurantId))
                .ForAllOtherMembers(opt => opt.Ignore());

            // Favorites Controller
            CreateMap<Deal, FavoriteDeal>()
                .ForMember(dest => dest.DealId, opts => opts.MapFrom(
                    src => src.DealId));

            CreateMap<ApplicationUser, FavoriteDeal>()
                .ForMember(dest => dest.UserId, opts => opts.MapFrom(
                    src => src.Id));

            // Purchases Controller
            CreateMap<PurchaseRequestDTO, Purchase>();
            CreateMap<RedeemPurchaseCodeRequestDTO, Purchase>();

            CreateMap<Purchase, PurchaseResponseDTO>()
                .ForMember(dest => dest.PurchaseCode, opt => opt.MapFrom(
                    src => src.PurchaseId.ToString()));

            CreateMap<ApplicationUser, Purchase>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(
                    src => src.Id))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<Deal, Purchase>()
                .ForMember(dest => dest.DealId, opt => opt.MapFrom(
                    src => src.DealId))
                .ForAllOtherMembers(opt => opt.Ignore());

            // Restaurants Controller
            CreateMap<RestaurantSubAccountCodeRequestDTO, Restaurant>();
            CreateMap<RestaurantRequestDTO, Restaurant>();
            CreateMap<Restaurant, RestaurantSummaryDTO>();

            CreateMap<Restaurant, RestaurantResponseDTO>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(
                    src => src.Owner != null ? src.Owner.Email ??  "" : ""))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(
                    src => src.Owner != null ? src.Owner.PhoneNumber ?? "" : ""))
                .ForMember(dest => dest.SubAccountCode, opt => opt.MapFrom(
                    src => src.SubAccountCode ?? ""));

            CreateMap<ApplicationUser, Restaurant>()
                .ForMember(dest => dest.OwnerId, opt => opt.MapFrom(
                    src => src.Id))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<City, Restaurant>()
                .ForMember(dest => dest.CityId, opt => opt.MapFrom(
                    src => src.CityId))
                .ForAllOtherMembers(opt => opt.Ignore());

            // Users Controller
            CreateMap<ApplicationUser, PartnerProfileDTO>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(
                    src => !src.IsDeleted ? src.Email ?? "" :
                            src.Email != null ? src.Email
                                                   .Split('?', StringSplitOptions.None)
                                                   .First() :
                            null))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(
                    src => src.PhoneNumber ?? ""))
                .ForMember(dest => dest.ProfilePictureUrl, opt => opt.MapFrom(
                    src => src.ProfilePictureUrl ?? ""));

            CreateMap<ApplicationUser, UserProfileCompleteDTO>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(
                    src => !src.IsDeleted ? src.Email ?? "" :
                            src.Email != null ? src.Email
                                                   .Split('?', StringSplitOptions.None)
                                                   .First() : 
                            null))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(
                    src => src.PhoneNumber ?? ""))
                .ForMember(dest => dest.ProfilePictureUrl, opt => opt.MapFrom(
                    src => src.ProfilePictureUrl ?? ""));

            CreateMap<ChangeProfilePictureDTO, ApplicationUser>()
                .ForMember(dest => dest.ProfilePictureUrl, opts => opts.MapFrom(
                    src => src.ProfilePictureUrl))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }

}
