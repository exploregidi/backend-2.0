﻿using System;
using System.Collections.Generic;

namespace explore_gidi_backend.Models
{
    public class Restaurant
    {
        public Guid RestaurantId { get; set; } = Guid.NewGuid();
        public string Slug { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string SubAccountCode { get; set; }

        public Guid CityId { get; set; }
        public virtual City City { get; set; }

        public Guid? OwnerId { get; set; }
        public virtual ApplicationUser Owner { get; set; }

        public virtual ICollection<Deal> Deals { get; set; } = new HashSet<Deal>();
    }
}
