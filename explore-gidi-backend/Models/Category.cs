﻿using System;
using System.Collections.Generic;

namespace explore_gidi_backend.Models
{
    public class Category
    {
        public Guid CategoryId { get; set; } = Guid.NewGuid();
        public string Slug { get; set; }
        public string Name { get; set; }
        public DateTime TimeDeleted { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Deal> Deals { get; set; } = new HashSet<Deal>();
    }
}
