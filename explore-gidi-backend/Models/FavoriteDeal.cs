﻿using System;

namespace explore_gidi_backend.Models
{
    public class FavoriteDeal
    {
        public Guid DealId { get; set; }
        public virtual Deal Deal { get; set; }
        public Guid UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
