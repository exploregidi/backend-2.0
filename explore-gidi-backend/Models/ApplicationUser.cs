﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace explore_gidi_backend.Models
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public ApplicationUser() : base()
        {
        }

        public ApplicationUser(string userName) : base(userName)
        {
        }

        public DateTime? DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string ProfilePictureUrl { get; set; }
        public bool IsDeleted { get; set; }
        public bool ShouldDelete { get; set; }
        public DateTime TimeCreated { get; set; } = DateTime.UtcNow;
        public DateTime? TimeDeleteRequestReceived { get; set; }
        public DateTime? TimeReactivated { get; set; }
        public DateTime? TimeDeleted { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<FavoriteDeal> FavoriteDeals { get; set; } = new HashSet<FavoriteDeal>();
        public virtual ICollection<Purchase> Purchases { get; set; } = new HashSet<Purchase>();
        public virtual ICollection<RefreshToken> RefreshTokens { get; set; } = new HashSet<RefreshToken>();
    }

}
