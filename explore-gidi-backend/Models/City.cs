﻿using System;
using System.Collections.Generic;

namespace explore_gidi_backend.Models
{
    public class City
    {
        public Guid CityId { get; set; } = Guid.NewGuid();
        public string Slug { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Restaurant> Restaurants { get; set; } = new HashSet<Restaurant>();
    }
}
