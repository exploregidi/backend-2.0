﻿using System;

namespace explore_gidi_backend.Models
{
    public class Purchase
    {
        public Guid PurchaseId { get; set; } = Guid.NewGuid();
        public string ReferenceNumber { get; set; }
        public DateTime TimeCreated { get; set; } = DateTime.Now;
        public bool IsRedeemed { get; set; }

        public Guid DealId { get; set; }
        public virtual Deal Deal { get; set; }

        public Guid UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
