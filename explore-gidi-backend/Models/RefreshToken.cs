﻿using System;

namespace explore_gidi_backend.Models
{
    public class RefreshToken
    {
        public string RefreshTokenId { get; set; }
        public DateTime ExpiryTime { get; set; }
        public DateTime GeneratedTime { get; set; }

        public Guid UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }

}
