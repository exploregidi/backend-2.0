﻿using System;
using System.Collections.Generic;

namespace explore_gidi_backend.Models
{
    public class Deal
    {
        public Guid DealId { get; set; } = Guid.NewGuid();
        public string Slug { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal ValuePrice { get; set; }
        public double PercentOff { get; set; }
        public DateTime TimeCreated { get; set; } = DateTime.Now;
        public DateTime ExpiryDate { get; set; }
        public string ImageUrl { get; set; }
        public bool IsDeleted { get; set; }

        public Guid? CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public Guid RestaurantId { get; set; }
        public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<FavoriteDeal> FavoriteDeals { get; set; } = new HashSet<FavoriteDeal>();
        public virtual ICollection<Purchase> Purchases { get; set; } = new HashSet<Purchase>();
    }
}
