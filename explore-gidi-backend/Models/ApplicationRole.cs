﻿using Microsoft.AspNetCore.Identity;
using System;

namespace explore_gidi_backend.Models
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string roleName) : base(roleName)
        {
        }
    }

}
