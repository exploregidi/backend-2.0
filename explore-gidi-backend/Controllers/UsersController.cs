﻿using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public ICityRepository CityRepository { get; }
        private IEmailService EmailService { get; set; }
        private IMapper Mapper { get; }
        public IRestaurantRepository RestaurantRepository { get; }
        public UserManager<ApplicationUser> UserManager { get; }
        public IUserRepository UserRepository { get; }

        public UsersController(ICityRepository cityRepository,
                               IEmailService emailService,
                               IMapper mapper,
                               IRestaurantRepository restaurantRepository,
                               UserManager<ApplicationUser> userManager,
                               IUserRepository userRepository)
        {
            CityRepository = cityRepository; 
            EmailService = emailService;
            Mapper = mapper;
            RestaurantRepository = restaurantRepository;
            UserManager = userManager;
            UserRepository = userRepository;
        }

        /// <summary>Get user endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Retrieves a specific user
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="403">Forbiddeb</response>
        /// <response code="404">Not Found</response>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{userId}")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<UserProfileCompleteDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(DataResponseDTO<PartnerProfileDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get([FromRoute]string userId)
        {
            ApplicationUser user;
            try
            {
                user = await UserManager.FindByIdAsync(userId);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error getting a user\n" +
                                                    $"UserId: {userId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            string currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (currentUserId != userId)
            {
                return Forbid();
            }

            if (User.IsInRole(UserRoleConstants.PARTNER))
            {
                user.Restaurant = RestaurantRepository.Get(x => x.OwnerId == user.Id)
                                                      .FirstOrDefault();

                var partnerProfileDTO = Mapper.Map<PartnerProfileDTO>(user);

                return Ok(new DataResponseDTO<PartnerProfileDTO>(partnerProfileDTO,
                    HttpStatusCode.OK));
            }
            else
            {
                var userProfileCompleteDTO = Mapper.Map<UserProfileCompleteDTO>(user);

                return Ok(new DataResponseDTO<UserProfileCompleteDTO>(userProfileCompleteDTO,
                    HttpStatusCode.OK));
            }
        }

        /// <summary>Change profile picture endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Accepts a url pointing to a user's profile picture
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPut("{userId}/profile-picture")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutProfilePicture([FromBody]ChangeProfilePictureDTO model,
                                                           [FromRoute]string userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            ApplicationUser user;
            try
            {
                user = await UserManager.FindByIdAsync(userId);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error getting a user\n" +
                                                    $"UserId: {userId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (currentUserId != user.Id.ToString())
            {
                return Forbid();
            }

            Mapper.Map(model, user);

            try
            {
                await UserManager.UpdateAsync(user);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error while updating profile-picture\n" +
                                                    $"UserId: {userId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Profile-picture Updated Successfully"));
        }

        /// <summary>Setup/Edit restaurant endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// This action creates a restaurant for the partner if one has not been created.
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{userId}/restaurant")]
        [Authorize(Roles = "Partner")]
        [ProducesResponseType(typeof(DataResponseDTO<RestaurantSummaryDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutPartnerRestaurant([FromRoute]string userId,
                                                              [FromBody]RestaurantRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            ApplicationUser user;
            try
            {
                user = await UserManager.FindByIdAsync(userId);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error getting a user\n" +
                                                    $"UserId: {userId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (currentUserId != user.Id.ToString())
            {
                return Forbid();
            }

            //if (!user.EmailConfirmed)
            //{
            //    return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
            //        new string[] { "Email not confirmed. Kindly confirm your email" +
            //            " and try again." }));
            //}

            var existingRestaurant = RestaurantRepository.Get(x => x.Name == model.Name &&
                                                                   x.OwnerId.ToString() != userId)
                                                         .FirstOrDefault();

            if (existingRestaurant != null)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Restaurant name unavailable, kindly provide a " +
                        "different name." }));
            }

            Restaurant restaurant;
            try
            {
                restaurant = RestaurantRepository.Get(x => x.OwnerId.ToString() == userId)
                                                 .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error getting a restaurant\n" +
                                                    $"PartnerId: {userId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            var city = CityRepository.Get(x => x.Slug == model.CitySlug)
                                     .FirstOrDefault();

            if (city == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "City not found" }));
            }

            if (restaurant == null)
            {
                restaurant = Mapper.Map<Restaurant>(model);
                Mapper.Map(user, restaurant);
                Mapper.Map(city, restaurant);

                RestaurantRepository.ConfigureDetails(restaurant);

                try
                {
                    RestaurantRepository.Insert(restaurant);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError($"Error creating a restaurant\n" +
                                                        $"PartnerId: {userId} \n" +
                                                        $"StackTrace: {ex.StackTrace} \n" +
                                                        $"TimeStamp: {DateTime.Now}");

                    return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                        new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
                }
            }
            else
            {
                bool isNewName = model.Name != restaurant.Name;
                Mapper.Map(model, restaurant);
                Mapper.Map(user, restaurant);
                Mapper.Map(city, restaurant);

                if (isNewName)
                {
                    RestaurantRepository.ConfigureDetails(restaurant);
                }

                try
                {
                    RestaurantRepository.Update(restaurant);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError($"Error updating a restaurant\n" +
                                                        $"PartnerId: {userId} \n" +
                                                        $"StackTrace: {ex.StackTrace} \n" +
                                                        $"TimeStamp: {DateTime.Now}");

                    return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                        new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
                }
            }

            Restaurant updatedRestaurant = RestaurantRepository.Get(x => x.Owner.Id == user.Id)
                                                               .FirstOrDefault();

            var restaurantDTO = Mapper.Map<RestaurantSummaryDTO>(updatedRestaurant);

            return Ok(new DataResponseDTO<RestaurantSummaryDTO>(restaurantDTO,
                HttpStatusCode.OK));
        }

        /// <summary>Delete user endpoint</summary>
        /// <remarks>Requires Authorization</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete([FromRoute]string userId)
        {
            ApplicationUser user;
            try
            {
                user = await UserManager.FindByIdAsync(userId);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error retrieving a user\n" +
                                                    $"UserId: {userId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != userId)
            {
                return Forbid();
            }

            try
            {
                UserRepository.FlagForDeletion(user);
                await UserManager.UpdateAsync(user);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error deleting a user\n" +
                                                    $"UserId: {user.Id} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            EmailService.SendAccountDeactivatedEmail(user.Email,
                $"{user.FirstName} {user.LastName}".Trim());

            return Ok(new DataResponseDTO<string>("Account deactivated"));
        }
    }
}