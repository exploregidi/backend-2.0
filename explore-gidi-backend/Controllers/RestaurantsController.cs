﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RestaurantsController : ControllerBase
    {
        private IMapper Mapper { get; }
        public IPurchaseRepository PurchaseRepository { get; }
        public IRestaurantRepository RestaurantRepository { get; }
        public UserManager<ApplicationUser> UserManager { get; }
        public IUserRepository UserRepository { get; }

        public RestaurantsController(IMapper mapper,
                                     IPurchaseRepository purchaseRepository,
                                     IRestaurantRepository restaurantRepository,
                                     UserManager<ApplicationUser> userManager,
                                     IUserRepository userRepository)
        {
            Mapper = mapper;
            PurchaseRepository = purchaseRepository;
            RestaurantRepository = restaurantRepository;
            UserManager = userManager;
            UserRepository = userRepository;
        }

        /// <summary>Get restaurants endpoint</summary>
        /// <remarks></remarks>
        /// <response code="200">Success</response>
        /// <param name="pageNumber"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(DataResponseArrayDTO<RestaurantSummaryDTO>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery]int pageNumber = 0,
                                 [FromQuery]int itemsPerPage = 20)
        {
            if (pageNumber < 0)
                pageNumber = 0;

            if (itemsPerPage < 0)
                itemsPerPage = 20;

            int skip = (itemsPerPage * pageNumber);

            IEnumerable<Restaurant> restaurants = null;
            try
            {
                restaurants = RestaurantRepository.Get(includeProperties: "Owner");

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrieving restaurants\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            var totalRestaurants = restaurants.Count();
            var pageRestaurants = restaurants.Skip(skip).Take(itemsPerPage).ToList();
            var restaurantsDTOs = Mapper.Map<List<RestaurantSummaryDTO>>(pageRestaurants);

            return Ok(new DataResponseArrayDTO<RestaurantSummaryDTO>(restaurantsDTOs,
                totalRestaurants, pageNumber, itemsPerPage));
        }

        /// <summary>Get specific restaurant endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Retrieves the restaurant with the specified slug
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="restaurantSlug"></param>
        /// <returns></returns>
        [HttpGet("{restaurantSlug}")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<RestaurantResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult GetRestaurant([FromRoute]string restaurantSlug)
        {
            Restaurant restaurant;
            try
            {
                restaurant = RestaurantRepository.Get(x => x.Slug == restaurantSlug,
                                                      includeProperties: "City," +
                                                                         "Owner")
                                                 .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error getting a restaurant\n" +
                                                    $"Slug: {restaurantSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (restaurant == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Restaurant not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != restaurant.OwnerId.ToString())
            {
                return Forbid();
            }

            var restaurantDTO = Mapper.Map<RestaurantResponseDTO>(restaurant);

            return Ok(new DataResponseDTO<RestaurantResponseDTO>(restaurantDTO, 
                HttpStatusCode.OK));
        }

        /// <summary>Get restaurant's purchases endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Retrieves a list of purchases for the specified restaurant
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="restaurantSlug"></param>
        /// <returns></returns>
        [HttpGet("{restaurantSlug}/purchases")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseArrayDTO<PurchaseResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult GetPurchases([FromRoute]string restaurantSlug)
        {
            Restaurant restaurant;
            try
            {
                restaurant = RestaurantRepository.Get(x => x.Slug == restaurantSlug)
                                                 .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error getting a restaurant\n" +
                                                    $"Slug: {restaurantSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (restaurant == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Restaurant not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != restaurant.OwnerId.ToString())
            {
                return Forbid();
            }

            var purchases = PurchaseRepository.Get(x => x.Deal.RestaurantId == restaurant.RestaurantId,
                                                   includeProperties: "Deal.Restaurant.City," +
                                                                      "User")
                                              .OrderByDescending(x => x.TimeCreated)
                                              .ToList();

            var purchaseDTOs = Mapper.Map<IEnumerable<PurchaseResponseDTO>>(purchases);

            return Ok(new DataResponseArrayDTO<PurchaseResponseDTO>(purchaseDTOs,
                purchaseDTOs.Count()));
        }
        
        /// <summary>Add/Edit restaurant sub account code endpoint</summary>
        /// <remarks>
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="restaurantSlug"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{restaurantSlug}/sub-account-code")]
        [Authorize(Roles = "Partner")]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult PutRestaurantSubAccountCode([FromRoute]string restaurantSlug,
                                                         [FromBody]RestaurantSubAccountCodeRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            Restaurant restaurant;
            try
            {
                restaurant = RestaurantRepository.Get(x => x.Slug == restaurantSlug)
                                                 .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error getting a restaurant\n" +
                                                    $"Slug: {restaurantSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (restaurant == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Restaurant not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != restaurant.OwnerId.ToString())
            {
                return Forbid();
            }

            Mapper.Map(model, restaurant);

            try
            {
                RestaurantRepository.Update(restaurant);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error updating a restaurant\n" +
                                                    $"Slug: {restaurant.Slug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Restaurant Sub Account Code " +
                "updated successfully"));
        }
    }
}