﻿using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DealsController : ControllerBase
    {
        public ICategoryRepository CategoryRepository { get; }
        public IDealRepository DealRepository { get; }
        public IFavoriteDealRepository FavoriteDealRepository { get; }
        private IMapper Mapper { get; }
        public IPurchaseRepository PurchaseRepository { get; }
        public IRestaurantRepository RestaurantRepository { get; }
        public UserManager<ApplicationUser> UserManager { get; }

        public DealsController(ICategoryRepository categoryRepository,
                               IDealRepository dealRepository,
                               IFavoriteDealRepository favoriteDealRepository,
                               IMapper mapper,
                               IPurchaseRepository purchaseRepository,
                               IRestaurantRepository restaurantRepository,
                               UserManager<ApplicationUser> userManager)
        {
            CategoryRepository = categoryRepository;
            DealRepository = dealRepository;
            FavoriteDealRepository = favoriteDealRepository;
            Mapper = mapper;
            PurchaseRepository = purchaseRepository;
            RestaurantRepository = restaurantRepository;
            UserManager = userManager;
        }

        /// <summary>Get deals endpoint</summary>
        /// <remarks>
        /// Retrieves a paginated list of all deals. &#xD;
        /// </remarks>
        /// <response code="200">Success</response>
        /// <param name="pageNumber"></param>
        /// <param name="itemsPerPage"></param>
        /// <param name="categorySlug">Specify a categorySlug to get only deals in a particular category</param>
        /// <param name="citySlug">Specify a citySlug to get only deals in a particular city</param>
        /// <param name="restaurantSlug">Specify a restaurantSlug to get only deals from a particular resturant</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(DataResponseArrayDTO<DealSummaryDTO>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery]int pageNumber = 0,
                                 [FromQuery]int itemsPerPage = 20,
                                 [FromQuery]string categorySlug = null,
                                 [FromQuery]string citySlug = null,
                                 [FromQuery]string restaurantSlug = null)
        {
            if (pageNumber < 0)
                pageNumber = 0;

            if (itemsPerPage < 0)
                itemsPerPage = 20;

            int skip = (itemsPerPage * pageNumber);
            List<Deal> deals = null;
            try
            {

                deals = DealRepository.Get(x => x.Category.Slug ==
                                                (categorySlug ?? x.Category.Slug) && 
                                                x.Restaurant.City.Slug ==
                                                (citySlug ?? x.Restaurant.City.Slug) &&
                                                x.Restaurant.Slug ==
                                                (restaurantSlug ?? x.Restaurant.Slug) &&
                                                !x.IsDeleted &&
                                                x.ExpiryDate >= DateTime.Now,
                                           includeProperties: "Category," +
                                                              "Restaurant.City," +
                                                              "Restaurant.Owner")
                                      .OrderBy(x => x.ExpiryDate)
                                      .ThenByDescending(x => x.TimeCreated)
                                      .ToList();

                if (deals.Count() < itemsPerPage)
                {
                    var expiredDeals = DealRepository.Get(x => x.Category.Slug ==
                                                               (categorySlug ?? x.Category.Slug) &&
                                                               x.Restaurant.City.Slug ==
                                                               (citySlug ?? x.Restaurant.City.Slug) &&
                                                               x.Restaurant.Slug ==
                                                               (restaurantSlug ?? x.Restaurant.Slug) &&
                                                               !x.IsDeleted &&
                                                               x.ExpiryDate < DateTime.Now,
                                                          includeProperties: "Category," +
                                                                             "Restaurant.City," +
                                                                             "Restaurant.Owner")
                                                     .OrderByDescending(x => x.ExpiryDate)
                                                     .ThenByDescending(x => x.TimeCreated)
                                                     .ToList();

                    deals.AddRange(expiredDeals);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrieving deals\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            var totalDeals = deals.Count();

            var pageDeals = deals.Skip(skip).Take(itemsPerPage).ToList();

            itemsPerPage = pageDeals.Count() < itemsPerPage ? pageDeals.Count() : itemsPerPage; 

            var dealDTOs = Mapper.Map<IEnumerable<DealSummaryDTO>>(pageDeals);

            return Ok(new DataResponseArrayDTO<DealSummaryDTO>(dealDTOs,
                totalDeals, pageNumber, itemsPerPage));
        }

        /// <summary>Get specific deal endpoint</summary>
        /// <remarks>Retrieves the deal with the specified slug</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Success</response>
        /// <response code="404">Not Found</response>
        /// <param name="dealSlug"></param>
        /// <returns></returns>
        [HttpGet("{dealSlug}")]
        [ProducesResponseType(typeof(DataResponseDTO<DealResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult GetDeal([FromRoute]string dealSlug)
        {
            Deal deal;
            try
            {
                deal = DealRepository.Get(x => x.Slug == dealSlug &&
                                               !x.IsDeleted,
                                          includeProperties: "Category," +
                                                             "Restaurant.City," +
                                                             "Restaurant.Owner")
                                       .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a deal\n" +
                                                    $"DealSlug: {dealSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (deal == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Deal not found" }));
            }

            var dealDTO = Mapper.Map<DealResponseDTO>(deal);

            string currentUserId = "";
            if (User.Identity.IsAuthenticated)
            {
                currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }

            var favoriteDeal = FavoriteDealRepository.Get(x => x.DealId == deal.DealId &&
                                                               x.UserId.ToString() == currentUserId)
                                                     .FirstOrDefault();

            Mapper.Map(favoriteDeal, dealDTO);

            return Ok(new DataResponseDTO<DealResponseDTO>(dealDTO, HttpStatusCode.OK));
        }

        /// <summary>Edit deal endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Success</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="dealSlug"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{dealSlug}")]
        [Authorize(Roles = "Partner")]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult Put([FromRoute]string dealSlug, [FromBody]DealRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            if (!Helper.IsValidDateString(model.ExpiryDate))
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Invalid date entered for ExpiryDate. " +
                        "Expected format dd/mm/yyyy" }));
            }

            Deal deal;
            try
            {
                deal = DealRepository.Get(x => x.Slug == dealSlug &&
                                               !x.IsDeleted,
                                          includeProperties: "Restaurant.Owner")
                                     .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a deal\n" +
                                                    $"DealSlug: {dealSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (deal == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Deal not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != deal.Restaurant.OwnerId.ToString())
            {
                return Forbid();
            }

            Mapper.Map(model, deal);

            try
            {
                DealRepository.Update(deal);
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError($"Error updating a deal\n" +
                                                    $"DealSlug: {deal.Slug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Deal updated successfully."));
        }
        
        /// <summary>Post deal endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Add a deal to the restaurant owned by the authorized user
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Success</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Partner")]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult Post([FromBody]DealRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            if (!Helper.IsValidDateString(model.ExpiryDate))
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Invalid date entered for ExpiryDate. " +
                        "Expected format dd/mm/yyyy" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            Category category;
            try
            {
                category = CategoryRepository.Get(x => x.Slug == (model.CategorySlug ?? ""))
                                             .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a category\n" +
                                                    $"UserId: {currentUserId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (category != null)
            {
                try
                {
                    category = CategoryRepository.Get(x => x.Name == AppConstants.FEATURED)
                                                 .FirstOrDefault();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError($"Error retrieving a category\n" +
                                                        $"UserId: {currentUserId} \n" +
                                                        $"StackTrace: {ex.StackTrace} \n" +
                                                        $"TimeStamp: {DateTime.Now}");

                    return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                        new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
                }
            }

            Restaurant restaurant;
            try
            {
                restaurant = RestaurantRepository.Get(x => x.OwnerId.ToString() == currentUserId,
                                                      includeProperties: "Owner")
                                                 .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a restaurant\n" +
                                                    $"UserId: {currentUserId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (restaurant == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Restaurant not set up. Set up your restaurant " +
                        "and try again" }));
            }

            var newDeal = Mapper.Map<Deal>(model);
            Mapper.Map(restaurant, newDeal);
            Mapper.Map(category, newDeal);

            DealRepository.ConfigureDetails(newDeal);

            try
            {
                DealRepository.Insert(newDeal);
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError($"Error inserting a deal\n" +
                                                    $"RestaurantSlug: {restaurant.Slug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Deal uploaded successfully."));
        }

        /// <summary>Delete deal endpoint</summary>
        /// <remarks>
        /// Requires Authorization
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="dealSlug"></param>
        /// <returns></returns>
        [HttpDelete("{dealSlug}")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult DeleteDeal([FromRoute]string dealSlug)
        {
            Deal deal;
            try
            {
                deal = DealRepository.Get(x => x.Slug == dealSlug &&
                                               !x.IsDeleted,
                                          includeProperties: "Restaurant.Owner")
                                       .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a deal\n" +
                                                    $"DealSlug: {dealSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Deal not found" }));
            }

            if (deal == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Deal not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != deal.Restaurant.OwnerId.ToString())
            {
                return Forbid();
            }

            var purchase = PurchaseRepository.Get(x => x.DealId == deal.DealId)
                                             .FirstOrDefault();
            
            if (purchase != null && !purchase.IsRedeemed)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "This deal has been purchased but not yet redeemed" }));
            }

            DealRepository.FlagDeletedDeal(deal);

            try
            {
                DealRepository.Update(deal);
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError($"Error Deleting a deal\n" +
                                                    $"DealSlug: {deal.Slug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Deal deleted successfully."));
        }
    }
}