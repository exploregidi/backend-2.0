﻿using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        public ICityRepository CityRepository { get; }
        private IMapper Mapper { get; }

        public CitiesController(ICityRepository cityRepository,
                                IMapper mapper)
        {
            CityRepository = cityRepository;
            Mapper = mapper;
        }

        /// <summary>Get cities endpoint</summary>
        /// <remarks>
        /// Retrieves a list of all cities with the latest 10 deals in each city
        /// </remarks>
        /// <response code="200">Success</response>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(DataResponseArrayDTO<CityResponseDTO>), StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            IEnumerable<City> cities = null;
            try
            {

                cities = CityRepository.Get(includeProperties: "Restaurants.Deals")
                                       .OrderBy(x => x.Name);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrievings cities\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            var cityDTOs = Mapper.Map<List<CityResponseDTO>>(cities);

            return Ok(new DataResponseArrayDTO<CityResponseDTO>(cityDTOs,
                cityDTOs.Count));
        }
    }
}