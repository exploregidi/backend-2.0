﻿using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/purchases")]
    [ApiController]
    public class PurchasesController : ControllerBase
    {
        public IDealRepository DealRepository { get; }
        private IMapper Mapper { get; }
        public IPurchaseRepository PurchaseRepository { get; }
        public UserManager<ApplicationUser> UserManager { get; }

        public PurchasesController(IDealRepository dealRepository,
                                   IMapper mapper,
                                   IPurchaseRepository purchaseRepository,
                                   UserManager<ApplicationUser> userManager)
        {
            DealRepository = dealRepository;
            Mapper = mapper;
            PurchaseRepository = purchaseRepository;
            UserManager = userManager;
        }

        /// <summary>Get purchases endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Retrieves a list of purchases for the authorized user
        /// </remarks>
        /// <response code="200">Success</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseArrayDTO<PurchaseResponseDTO>), StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            IEnumerable<Purchase> purchases = null;
            try
            {

                purchases = PurchaseRepository.Get(x => x.UserId.ToString() == currentUserId,
                                                   includeProperties: "Deal.Restaurant.Owner," +
                                                                      "Deal.Restaurant.City")
                                              .OrderByDescending(x => x.TimeCreated);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrieving purchases\n" +
                                                    $"UserId: {currentUserId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            var purchaseDTOs = Mapper.Map<IEnumerable<PurchaseResponseDTO>>(purchases);

            return Ok(new DataResponseArrayDTO<PurchaseResponseDTO>(purchaseDTOs,
                purchaseDTOs.Count()));
        }

        /// <summary>Get purchase endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="purchaseCode"></param>
        /// <returns></returns>
        [HttpGet("{purchaseCode}")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<PurchaseResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult GetSpecificPurchase([FromRoute]string purchaseCode)
        {
            Purchase purchase;
            try
            {
                purchase = PurchaseRepository.Get(x => x.PurchaseId.ToString() == purchaseCode,
                                                  includeProperties: "Deal.Restaurant," +
                                                                     "User")
                                             .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a purchase\n" +
                                                    $"PurchaseId: {purchaseCode} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (purchase == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Purchase not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != purchase.Deal.Restaurant.OwnerId.ToString() &&
                currentUserId != purchase.User.Id.ToString())
            {
                return Forbid();
            }

            var purchaseDTO = Mapper.Map<PurchaseResponseDTO>(purchase);

            return Ok(new DataResponseDTO<PurchaseResponseDTO>(purchaseDTO, HttpStatusCode.OK));
        }

        /// <summary>Redeem purchase endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <param name="purchaseCode"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{purchaseCode}")]
        [Authorize(Roles = "Partner")]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ForbidResult), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public IActionResult Put([FromRoute]string purchaseCode,
                                 [FromBody]RedeemPurchaseCodeRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            Purchase purchase;
            try
            {
                purchase = PurchaseRepository.Get(x => x.PurchaseId.ToString() == purchaseCode,
                                                  includeProperties: "Deal.Restaurant," +
                                                                     "User")
                                             .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a purchase\n" +
                                                    $"PurchaseId: {purchaseCode} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (purchase == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Purchase not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (currentUserId != purchase.Deal.Restaurant.OwnerId.ToString())
            {
                return Forbid();
            }

            Mapper.Map(model, purchase);

            try
            {
                PurchaseRepository.Update(purchase);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error updating a purchase\n" +
                                                    $"PurchaseId: {purchase.PurchaseId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Purchase Status updated successfully"));
        }

        /// <summary>Post purchase endpoint</summary>
        /// <remarks>
        /// Requires Authorization &#xD;
        /// Adds a purchase to the authorized user's purchases
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Success</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Post([FromBody]PurchaseRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await UserManager.FindByIdAsync(currentUserId);

            Deal deal;
            try
            {
                deal = DealRepository.Get(x => x.Slug == model.DealSlug &&
                                               !x.IsDeleted)
                                     .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"Error retrieving a deal\n" +
                                                    $"DealSlug: {model.DealSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (deal == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Deal not found" }));
            }
            
            if (deal.ExpiryDate < DateTime.Now)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Expired deal" }));
            }

            var purchase = Mapper.Map<Purchase>(model);
            Mapper.Map(user, purchase);
            Mapper.Map(deal, purchase);

            try
            {
                PurchaseRepository.Insert(purchase);
            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.TraceError($"Error inserting a purchase\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("Deal purchased successfully"));
        }
    }
}