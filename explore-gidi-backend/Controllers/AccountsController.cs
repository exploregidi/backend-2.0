﻿using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        public IAccountService AccountService { get; }
        public IConfiguration Configuration { get; }
        private IEmailService EmailService { get; set; }
        private IMapper Mapper { get; }
        public IRefreshTokenRepository RefreshTokenRepository { get; }
        public UserManager<ApplicationUser> UserManager { get; }
        public IUserRepository UserRepository { get; }

        public AccountsController(IAccountService accountService,
                                  IConfiguration configuration,
                                  IEmailService emailService,
                                  IMapper mapper,
                                  IRefreshTokenRepository refreshTokenRepository, 
                                  UserManager<ApplicationUser> userManager,
                                  IUserRepository userRepository)
        {
            AccountService = accountService;
            Configuration = configuration;
            EmailService = emailService;
            Mapper = mapper; 
            RefreshTokenRepository = refreshTokenRepository;
            UserManager = userManager;
            UserRepository = userRepository;
        }

        /// <summary>User registration endpoint</summary>
        /// <remarks>
        /// Requires a unique email
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("register")]
        [ProducesResponseType(typeof(DataResponseDTO<LoginResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostApplicationUser([FromBody]UserRegisterDTO model)
        {
            AccountService.DeleteDeactivatedAccounts();

            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            ApplicationUser applicationUser = await UserManager.FindByEmailAsync(model.Email);

            if (applicationUser != null && string.Equals(applicationUser.Email.Split(".").First(), "deleted"))
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Invalid email addres" }));
            }
            
            if (applicationUser != null)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "The email has been registered already" }));
            }

            applicationUser = Mapper.Map<ApplicationUser>(model);

            try
            {
                await UserManager.CreateAsync(applicationUser, model.Password);
                await UserManager.AddToRoleAsync(applicationUser, UserRoleConstants.USER);

                if (model.IsAPartner)
                {
                    await UserManager.AddToRoleAsync(applicationUser, UserRoleConstants.PARTNER);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("There was an error registering a User\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            EmailService.SendSuccessfulRegistrationMessage(applicationUser.Email,
                $"{applicationUser.FirstName} {applicationUser.LastName}".Trim());

            //if (model.IsAPartner)
            //{
            //    string confirmEmailToken = await UserManager.GenerateEmailConfirmationTokenAsync(applicationUser);

            //    EmailService.SendConfirmEmail(applicationUser.Email, $"{applicationUser.FirstName} " +
            //        $"{applicationUser.LastName}".Trim(), confirmEmailToken);
            //}

            return Ok(GetJWTToken(applicationUser));
        }

        /// <summary>Login endpoint</summary>
        /// <remarks></remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(typeof(DataResponseDTO<LoginResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login([FromBody]LoginRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            ApplicationUser user = null;

            try
            {
                user = await UserManager.FindByEmailAsync(model.Email);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("There was an error logging a User in\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
                
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (user == null)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new List<string> { "The username or password is incorrect" }));
            }

            var validPassword = await UserManager.CheckPasswordAsync(user, model.Password);
            if (!validPassword)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new List<string> { "The username or password is incorrect" }));
            }

            if (user.ShouldDelete)
            {
                AccountService.ReactivateAccount(user);

                EmailService.SendAccountReactivatedEmail(user.Email, $"{user.FirstName} " +
                    $"{user.LastName}".Trim());
            }

            return Ok(GetJWTToken(user));
        }

        /// <summary>Refresh token endpoint</summary>
        /// <remarks>Requires a valid refreshtoken and userId</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("refresh-token")]
        [ProducesResponseType(typeof(DataResponseDTO<LoginResponseDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RefreshToken([FromBody]RefreshTokenDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            var user = await UserManager.FindByIdAsync(model.UserId);
            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "The user was not found" }));
            }

            var refreshToken = RefreshTokenRepository.Get(x => x.RefreshTokenId == model.RefreshToken && 
                                                        x.ExpiryTime > DateTime.UtcNow)
                                              .FirstOrDefault();

            if (refreshToken != null)
            {
                RefreshTokenRepository.Delete(model.RefreshToken);
                return Ok(GetJWTToken(user));
            }

            return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                new string[] { "Invalid or expired refresh token" }));
        }

        /// <summary>Logout endpoint</summary>
        /// <remarks>Requires Authorization</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("logout")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateErrorResponseDTO), StatusCodes.Status400BadRequest)]
        public IActionResult Logout([FromBody]LogoutRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var token = RefreshTokenRepository.Get(x => x.RefreshTokenId == model.RefreshToken && 
                                                        x.User.Id == new Guid(currentUserId))
                                              .FirstOrDefault();

            if (token != null)
            {
                RefreshTokenRepository.Delete(model.RefreshToken);
            }

            return Ok(new DataResponseDTO<string>("Logout successful"));
        }

        /// <summary>Change password endpoint</summary>
        /// <remarks>Requires Authorization</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("password/change")]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await UserManager.FindByIdAsync(currentUserId);

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "The user was not found" }));
            }

            IdentityResult result;
            try
            {
                result = await UserManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
                
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error while changing password\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (result != null && result.Succeeded)
            {
                EmailService.SendPasswordChangedEmail(user.Email,
                    $"{user.FirstName} {user.LastName}".Trim());

                return Ok(new DataResponseDTO<string>("Password changed successfully"));
            }
            else
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] {"The current password entered is incorrect. Kindly navigate " +
                                  "to the forgot password page (if you cannot remember your current password) " +
                                  "to reset your password or contact the support team." }));
            }
        }

        /// <summary>Get new email confirmation token endpoint</summary>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("confirm-email/new-token")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetNewConfirmationToken([FromBody]ResendConfirmEmailTokenDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            ApplicationUser user;
            try
            {
                user = await UserManager.FindByEmailAsync(model.Email);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error getting a user\n" +
                                                    $"Email: {model.Email} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            string confirmEmailToken;
            try
            {
                confirmEmailToken = await UserManager.GenerateEmailConfirmationTokenAsync(user);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error generation email confirmation " +
                                                    "token for a user\n" +
                                                    $"UserId: {user.Id} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            EmailService.SendConfirmEmail(user.Email, $"{user.FirstName} " +
                    $"{user.LastName}".Trim(), confirmEmailToken);

            return Ok(new DataResponseDTO<string>("Success. Check your mailbox for a " +
                "confirmation token"));
        }

        /// <summary>Confirm email endpoint</summary>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("confirm-email")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ConfirmEmail([FromBody]ConfirmEmailDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            ApplicationUser user;
            try
            {
                user = await UserManager.FindByEmailAsync(model.Email);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error getting a user\n" +
                                                    $"Email: {model.Email} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            IdentityResult result;
            try
            {
                result = await UserManager.ConfirmEmailAsync(user, model.Token);

                if (result.Succeeded)
                {
                    user.EmailConfirmed = true;
                    await UserManager.UpdateAsync(user);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error Confirming a user email\n" +
                                                    $"UserId: {user.Id} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (result.Succeeded)
            {
                return Ok(new DataResponseDTO<string>("Email Confirmed Successfully."));
            }
            else
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Invalid Token" }));
            }
        }

        /// <summary>Forgot password endpoint</summary>
        /// <remarks></remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("password/forgot")]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            var user = await UserManager.FindByEmailAsync(model.Email);

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            var passwordResetToken = await UserManager.GeneratePasswordResetTokenAsync(user);

            EmailService.SendForgotPasswordEmail(user.Email,
                $"{user.FirstName} {user.LastName}".Trim(), passwordResetToken);

            return Ok(new DataResponseDTO<string>("An email has been sent to you with details " +
                "on how to reset your password"));
        }

        /// <summary>Reset password endpoint</summary>
        /// <remarks></remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("password/reset")]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            var user = await UserManager.FindByEmailAsync(model.Email);

            if (user == null || user.ShouldDelete)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "User not found" }));
            }

            IdentityResult result;
            try
            {
                result = await UserManager.ResetPasswordAsync(user, model.PasswordResetToken, model.Password);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error while reseting password\n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
                
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "Could not complete request. Please retry later, " +
                        "or contact the support team" }));
            }

            if (result != null && result.Succeeded)
            {
                EmailService.SendPasswordResetEmail(user.Email,
                    $"{user.FirstName} {user.LastName}".Trim());

                return Ok(new DataResponseDTO<string>("Your password was reset successfully"));
            }
            else
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] {"Invalid reset password token. Kindly request for another " +
                        "reset password token or contact the support team." }));
            }
        }

        private DataResponseDTO<LoginResponseDTO> GetJWTToken(ApplicationUser user)
        {
            var currentTime = DateTime.UtcNow;
            var userRoles = UserManager.GetRolesAsync(user).Result;
            IdentityOptions identityOptions = new IdentityOptions();
            var claims = new List<Claim>
            {
                new Claim(identityOptions.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                new Claim(identityOptions.ClaimsIdentity.UserNameClaimType, user.Email)
            };
            foreach (var role in userRoles)
            {
                claims.Add(new Claim(identityOptions.ClaimsIdentity.RoleClaimType, role));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims.ToArray()),
                Expires = currentTime.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(
                        Configuration["JWT_Secret"].ToString())
                    ), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);

            var refreshToken = Mapper.Map<RefreshToken> (user);

            do
            {
                refreshToken.RefreshTokenId = Helper.GetRandomToken(96);
            }
            while (RefreshTokenRepository.GetByID(refreshToken.RefreshTokenId) != null);

            RefreshTokenRepository.Insert(refreshToken);

            var loginResponseDTO = Mapper.Map<LoginResponseDTO>(user);

            loginResponseDTO.RefreshToken = refreshToken.RefreshTokenId;
            loginResponseDTO.ExpiryTime = tokenDescriptor.Expires.ToString();
            loginResponseDTO.Token = token;
            loginResponseDTO.Roles = userRoles;

            return new DataResponseDTO<LoginResponseDTO>(loginResponseDTO);
        }
    }
}