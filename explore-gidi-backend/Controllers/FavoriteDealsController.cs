﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using explore_gidi_backend.Models;
using explore_gidi_backend.ModelsDTO.Requests;
using explore_gidi_backend.ModelsDTO.Responses;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace explore_gidi_backend.Controllers
{
    [Route("api/v1/favorite-deals")]
    [ApiController]
    public class FavoriteDealsController : ControllerBase
    {
        public IDealRepository DealRepository { get; }
        public IFavoriteDealRepository FavoriteDealRepository { get; }
        private IMapper Mapper { get; }
        public UserManager<ApplicationUser> UserManager { get; }

        public FavoriteDealsController(IDealRepository dealRepository,
                                       IFavoriteDealRepository favoriteDealRepository,
                                       UserManager<ApplicationUser> userManager,
                                       IMapper mapper)
        {
            DealRepository = dealRepository;
            FavoriteDealRepository = favoriteDealRepository;
            Mapper = mapper;
            UserManager = userManager;
        }

        /// <summary>Get favorite deals endpoint</summary>
        /// <remarks>
        /// Requires authorization. &#xD;
        /// Returns the favorite deals for the authorized user
        /// </remarks>
        /// <response code="200">Success</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseArrayDTO<DealSummaryDTO>), StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            
            List<Deal> deals = null;
            List<Deal> expiredDeals = null;
            try
            {
                deals = FavoriteDealRepository.Get(x => x.UserId.ToString() == currentUserId &&
                                                        x.Deal.ExpiryDate >= DateTime.Now,
                                                   includeProperties:"Deal.Restaurant.City")
                                              .Select(x => x.Deal)
                                              .OrderBy(x => x.ExpiryDate)
                                              .ThenByDescending(x => x.TimeCreated)
                                              .ToList();

                expiredDeals = FavoriteDealRepository.Get(x => x.UserId.ToString() == currentUserId &&
                                                               x.Deal.ExpiryDate < DateTime.Now,
                                                          includeProperties: "Deal.Restaurant.City")
                                                     .Select(x => x.Deal)
                                                     .OrderByDescending(x => x.ExpiryDate)
                                                     .ThenByDescending(x => x.TimeCreated)
                                                     .ToList();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrieving favoritedeals\n" +
                                                    $"UserId: {currentUserId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            deals.AddRange(expiredDeals);

            var dealsDTOs = Mapper.Map<List<DealSummaryDTO>>(deals);

            return Ok(new DataResponseArrayDTO<DealSummaryDTO>(dealsDTOs,
                dealsDTOs.Count));
        }

        /// <summary>Post favorite deal endpoint</summary>
        /// <remarks>
        /// Requires authorization &#xD;
        /// Add a deal to the authorized user's favorites
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(DataResponseDTO<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDTO), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Post([FromBody]FavoriteDealRequestDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ModelStateErrorResponseDTO(HttpStatusCode.BadRequest,
                    ModelState));
            }

            Deal deal = null;
            try
            {
                deal = DealRepository.Get(x => x.Slug == model.DealSlug &&
                                               !x.IsDeleted)
                                     .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrieving a deal\n" +
                                                    $"DealSlug: {model.DealSlug} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            if (deal == null)
            {
                return NotFound(new ErrorResponseDTO(HttpStatusCode.NotFound,
                    new string[] { "Deal not found" }));
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var user = await UserManager.FindByIdAsync(currentUserId);

            FavoriteDeal existingFavoriteDeal = null;
            try
            {
                existingFavoriteDeal = FavoriteDealRepository.Get(x => x.DealId == deal.DealId &&
                                                                       x.UserId == user.Id)
                                                             .FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error retrieving a favoritedeal\n" +
                                                    $"DealId: {deal.DealId} \n" +
                                                    $"UserId: {user.Id} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            if (existingFavoriteDeal != null)
            {
                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { "This deal is already in your favorites" }));
            }

            var favoriteDeal = Mapper.Map<FavoriteDeal>(user);
            Mapper.Map(deal, favoriteDeal);

            try
            {
                FavoriteDealRepository.Insert(favoriteDeal);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError($"There was an error posting a favoriteDeal\n" +
                                                    $"DealId: {favoriteDeal.DealId} \n" +
                                                    $"UserId: {favoriteDeal.UserId} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");

                return BadRequest(new ErrorResponseDTO(HttpStatusCode.BadRequest,
                    new string[] { AppConstants.DEFAULT_ERROR_MESSAGE }));
            }

            return Ok(new DataResponseDTO<string>("You have successfully added this " +
                "deal to your favorites"));
        }
    }
}