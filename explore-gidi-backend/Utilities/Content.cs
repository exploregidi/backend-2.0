﻿using explore_gidi_backend.Models;
using System.Collections.Generic;

namespace explore_gidi_backend.Utilities
{
    /// <summary>
    /// Returns the actual data for a model
    /// </summary>
    /// <returns></returns>
    public class Content
    {

        /// <summary>
        /// Returns a list of Categories
        /// </summary>
        /// <returns></returns>
        public static List<Category> Categories()
        {
            string[] catagoryNames = {
                AppConstants.BRUNCH, 
                AppConstants.DINNER, 
                AppConstants.DRINKS,
                AppConstants.EVENTS,
                AppConstants.FEATURED,
                AppConstants.FOOD, 
                AppConstants.HAPPY_HOUR
            };

            var categoriesList = new List<Category>();

            foreach (var name in catagoryNames)
            {
                categoriesList.Add(new Category
                {
                    Name = name,
                    Slug = RegexUtilities.ToUrlSlug(name)
                });
            }

            return categoriesList;
        }

        /// <summary>
        /// Returns a list of Cities
        /// </summary>
        /// <returns></returns>
        public static List<City> Cities()
        {
            string[] cityNames = {
                //AppConstants.AJAH, 
                //AppConstants.GBAGADA, 
                AppConstants.IKEJA, 
                AppConstants.IKOYI, 
                //AppConstants.LAGOS_ISLAND, 
                AppConstants.LEKKI, 
                //AppConstants.MARYLAND, 
                //AppConstants.SURULERE, 
                AppConstants.VICTORIA_ISLAND, 
                //AppConstants.YABA, 
                // AppConstants.OTHER
            };

            var citiesList = new List<City>();

            foreach (var name in cityNames)
            {
                citiesList.Add(new City
                {
                    Name = name,
                    Slug = RegexUtilities.ToUrlSlug(name)
                });
            }

            return citiesList;
        }
    }
}
