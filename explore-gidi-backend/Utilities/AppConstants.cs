﻿namespace explore_gidi_backend.Utilities
{
    public class AppConstants
    {
        // Categories
        //Drinks, Food, Dinner, Happy hour, Events, Brunch
        public static string BRUNCH = "Brunch";
        public static string DINNER = "Dinner";
        public static string DRINKS = "Drinks";
        public static string EVENTS = "Events";
        public static string FEATURED = "Featured";
        public static string FOOD = "Food";
        public static string HAPPY_HOUR = "Happy Hour";

        // Cities
        public static string AJAH = "Ajah";
        public static string GBAGADA = "Gbagada";
        public static string IKEJA = "Ikeja";
        public static string IKOYI = "Ikoyi";
        public static string LAGOS_ISLAND = "Lagos Island";
        public static string LEKKI = "Lekki";
        public static string MARYLAND = "MaryLand";
        public static string SURULERE = "Surulere";
        public static string VICTORIA_ISLAND = "Victoria Island";
        public static string YABA = "Yaba";

        // IMAGE URLS
        public static string MOON_PICTURE_URL = "https://cdn.pixabay.com/photo/2019/09/04/05/53/moon-4450739_1280.jpg";
        public static string CAFE_BAR_IMAGE_URL = "https://cdn.pixabay.com/photo/2019/11/05/00/25/cafe-bar-4602458_1280.jpg";

        // Numbers
        public static int NEGATIVE_SEVEN = -7;
        public static int NEGATIVE_TWO = -2;
        public static int NEGATIVE_ONE = -1;
        public static int TWO = 2;
        public static int TEN = 10;
        public static int ONE_HUNDRED = 100;

        // Others
        public static string DEFAULT_ERROR_MESSAGE = "Could not complete your registration. " +
            "Please retry later, or contact the support team";
        public static string DEFAULT_SUB_ACCOUNT_CODE = "ACCT_mkhox16qkvxcp2b";
        public static string OTHER = "Other";

        // Prices
        public static decimal SEVEN_HUNDRED = (decimal)700.00;
        public static decimal SIX_HUNDRED = (decimal)600.00;
        public static decimal THREE_THOUSAND_AND_TEN = (decimal)3010.00;
        public static decimal SIX_THOUSAND_DOT_FIVE_FIVE = (decimal)6000.5555;
    }
}
