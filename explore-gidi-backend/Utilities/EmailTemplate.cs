﻿namespace explore_gidi_backend.Utilities
{
    /// <summary>
    /// All the available email templates
    /// </summary>
    public sealed class EmailTemplate
    {
        /// <summary>
        /// The filename for this template 
        /// </summary>
        public string filename { get; }

        private EmailTemplate(string filename)
        {
            this.filename = filename;
        }

        /// <summary>
        /// A user's account has been deactivated and scheduled for deletion
        /// </summary>
        public static readonly EmailTemplate ACCOUNT_DEACTIVATED = new EmailTemplate("AccountDeactivated.html");

        /// <summary>
        /// A user's account has been reactivated
        /// </summary>
        public static readonly EmailTemplate ACCOUNT_REACTIVATED = new EmailTemplate("AccountReactivated.html");

        /// <summary>
        /// An email containing instructions to confirm the user's email
        /// </summary>
        public static readonly EmailTemplate CONFIRM_EMAIL = new EmailTemplate("ConfirmEmail.html");

        /// <summary>
        /// An user's account has been deleted
        /// </summary>
        public static readonly EmailTemplate DEAL_ABOUT_TO_EXPIRE = new EmailTemplate("DealAboutToExpire.html");
        
        /// <summary>
        /// An user's account has been deleted
        /// </summary>
        public static readonly EmailTemplate DELETE_ACCOUNT_SUCCESSFUL = new EmailTemplate("DeleteAccountSuccessful.html");

        /// <summary>
        /// A deal has expired
        /// </summary>
        public static readonly EmailTemplate EXPIRED_DEAL = new EmailTemplate("ExpiredDeal.html");

        /// <summary>
        /// An email containing instructions to reset the user password
        /// </summary>
        public static readonly EmailTemplate FORGOT_PASSWORD = new EmailTemplate("ForgotPassword.html");

        /// <summary>
        /// A user's password has successfully been changed
        /// </summary>
        public static readonly EmailTemplate PASSWORD_CHANGED_ALERT = new EmailTemplate("PasswordChangedAlert.html");

        /// <summary>
        /// The user has successfully reset their password
        /// </summary>
        public static readonly EmailTemplate PASSWORD_RESET_SUCCESSFUL = new EmailTemplate("PasswordResetSuccessful.html");

        /// <summary>
        /// An email welcoming a new user
        /// </summary>
        public static readonly EmailTemplate REGISTRATION_SUCCESSFUL = new EmailTemplate("RegistrationSuccessful.html");
    }
}
