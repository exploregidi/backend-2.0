﻿namespace explore_gidi_backend.Utilities
{
    public class UserRoleConstants
    {
        public static readonly string ADMIN = "Admin";
        public static readonly string PARTNER = "Partner";
        public static readonly string SUPER_ADMIN = "Super Admin";
        public static readonly string USER = "User";

        public static bool IsValidRole(string role)
        {
            return ADMIN == role || 
                   PARTNER == role || 
                   SUPER_ADMIN == role || 
                   USER == role;
        }
    }
}
