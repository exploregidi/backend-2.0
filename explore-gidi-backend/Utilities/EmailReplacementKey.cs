﻿namespace explore_gidi_backend.Utilities
{
    /// <summary>
    /// A class containing the values that can be replaced within an email template
    /// </summary>
    public sealed class EmailReplacementKey
    {
        /// <summary>
        /// The key string of this EmailReplacementKey
        /// </summary>
        public string KeyString { get; }

        private EmailReplacementKey(string key)
        {
            this.KeyString = key;
        }

        /// <summary>
        /// Deal's expiry date
        /// </summary>
        public static readonly EmailReplacementKey DEAL_EXPIRY_DATE = new EmailReplacementKey("{{deal-expiry-date}}");
        /// <summary>
        /// Deal's name
        /// </summary>
        public static readonly EmailReplacementKey DEAL_NAME = new EmailReplacementKey("{{deal-name}}");
        /// <summary>
        /// A URL link that takes the user straight to the homepage
        /// </summary>
        public static readonly EmailReplacementKey SITE_URL = new EmailReplacementKey("{{site-url}}");
        /// <summary>
        /// A timed one time password that the user uses to create a new password
        /// </summary>
        public static readonly EmailReplacementKey TIME_ONE_TIME_PASSWORD = new EmailReplacementKey("{{timed-one-time-password}}");
        /// <summary>
        /// The user's username
        /// </summary>
        public static readonly EmailReplacementKey USERNAME = new EmailReplacementKey("{{username}}");
        /// <summary>
        /// A URL link that takes the user straight to his profile page
        /// </summary>
        public static readonly EmailReplacementKey USER_PROFILE_URL = new EmailReplacementKey("{{user-profile-url}}");
        /// <summary>
        /// A URL link that the user uses to verify email
        /// </summary>
        public static readonly EmailReplacementKey VERIFY_EMAIL_URL = new EmailReplacementKey("{{verify-email-url}}");
    }
}
