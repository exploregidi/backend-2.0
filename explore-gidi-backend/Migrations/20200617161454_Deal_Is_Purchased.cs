﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace explore_gidi_backend.Migrations
{
    public partial class Deal_Is_Purchased : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPurchased",
                table: "Deals");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPurchased",
                table: "Deals",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }
    }
}
