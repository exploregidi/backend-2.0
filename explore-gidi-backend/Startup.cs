using AutoMapper;
using explore_gidi_backend.AutoMapper;
using explore_gidi_backend.DBContexts;
using explore_gidi_backend.DBInitializers;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Implementations;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Services.Implementations;
using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace explore_gidi_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            services.AddMvc(options => options.EnableEndpointRouting = false);

            services.AddCors();

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Tokens.PasswordResetTokenProvider = TokenOptions.DefaultEmailProvider;
                options.Tokens.EmailConfirmationTokenProvider = TokenOptions.DefaultEmailProvider;
            }).AddEntityFrameworkStores<MariaDbContext>().AddDefaultTokenProviders();

            // Automapper
            var mappingConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            // CronJobs
            services.AddCronJob<DailyJobs>(c =>
            {
                c.TimeZoneInfo = TimeZoneInfo.Local;
                // 2:30 am 
                c.CronExpression = @"30 2 * * *";
            });

            // MariaDB
            services.AddDbContext<MariaDbContext>(options =>
                options.UseMySql(Configuration["MariaDbConnectionString"].ToString(),
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(10, 4, 8), ServerType.MariaDb);
                    }
            ), ServiceLifetime.Transient, ServiceLifetime.Transient);

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
            });

            //Jwt Authentication
            var key = Encoding.UTF8.GetBytes(Configuration["JWT_Secret"].ToString());

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddRouting(options => options.LowercaseUrls = true);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            // Swagger
            string swaggerConfiguration = Configuration["ShouldShowSwaggerDocs"];

            if (swaggerConfiguration != null)
            {
                bool shouldShowSwaggerDocs;
                bool.TryParse(swaggerConfiguration, out shouldShowSwaggerDocs);

                if (shouldShowSwaggerDocs)
                {
                    services.AddSwaggerGen(c =>
                    {
                        c.SwaggerDoc("v1", new OpenApiInfo
                        {
                            Title = "Gidi Eats Backend APIs V1",
                            Version = "v1",
                            Description = "This is a private document, for Gidi Eats internal use only." +
                                "\nFor API consumers, always ensure that you handle 500 errors." +
                                "These indicate a Server Error, and we would like to know about these ASAP."
                        });

                        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFile));

                    });
                }
            }

            // Repositories
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddTransient<IDealRepository, DealRepository>();
            services.AddScoped<IFavoriteDealRepository, FavoriteDealRepository>();
            services.AddTransient<IPurchaseRepository, PurchaseRepository>();
            services.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddScoped<IRestaurantRepository, RestaurantRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            // Services
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IDealService, DealService>();
            services.AddTransient<IEmailService, EmailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                              IWebHostEnvironment env,
                              ILoggerFactory loggerFactory,
                              ICategoryRepository categoryRepository,
                              ICityRepository cityRepository,
                              IDealRepository dealRepository,
                              IRestaurantRepository restaurantRepository,
                              RoleManager<ApplicationRole> roleManager,
                              UserManager<ApplicationUser> userManager)
        {
            // loggerFactory.AddFile(Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseMvc();

            string swaggerConfiguration = Configuration["ShouldShowSwaggerDocs"];

            if (swaggerConfiguration != null)
            {
                bool shouldShowSwaggerDocs;
                bool.TryParse(swaggerConfiguration, out shouldShowSwaggerDocs);

                if (shouldShowSwaggerDocs)
                {
                    app.UseSwagger();

                    app.UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Gidi Eats Backend APIs V1");
                    });
                }
            }

            Helper.Configuration = Configuration;

            // Migrate Database
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<MariaDbContext>().Database.Migrate();
            }

            // Seed Database
            AppDbInitializer.SeedDatabase(Configuration,
                                          loggerFactory.CreateLogger<AppDbInitializer>(),
                                          categoryRepository,
                                          cityRepository,
                                          dealRepository,
                                          restaurantRepository,
                                          roleManager,
                                          userManager);
        }
    }
}
