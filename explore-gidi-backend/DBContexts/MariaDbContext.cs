﻿using explore_gidi_backend.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace explore_gidi_backend.DBContexts
{
    public class MariaDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public virtual DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public virtual DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Deal> Deals { get; set; }
        public virtual DbSet<FavoriteDeal> FavoriteDeals { get; set; }
        public virtual DbSet<Purchase> Purchases { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public virtual DbSet<Restaurant> Restaurants { get; set; }
        public virtual DbSet<IdentityUserLogin<Guid>> UserLogins { get; set; }

        public MariaDbContext(DbContextOptions<MariaDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                .HasIndex(bc => new { bc.Email })
                .IsUnique();

            modelBuilder.Entity<ApplicationUser>()
                .HasIndex(bc => new { bc.UserName })
                .IsUnique();

            modelBuilder.Entity<Category>()
                .HasKey(bc => new { bc.CategoryId });

            modelBuilder.Entity<Category>()
                .HasIndex(bc => new { bc.Slug })
                .IsUnique();
            
            modelBuilder.Entity<City>()
                .HasKey(bc => new { bc.CityId });

            modelBuilder.Entity<City>()
                .HasIndex(bc => new { bc.Slug })
                .IsUnique();

            modelBuilder.Entity<Deal>()
                .HasKey(bc => new { bc.DealId });

            modelBuilder.Entity<Deal>()
                .HasIndex(bc => new { bc.Slug })
                .IsUnique();

            modelBuilder.Entity<Deal>()
                .HasOne(bc => bc.Category)
                .WithMany(b => b.Deals)
                .HasForeignKey(bc => bc.CategoryId);
            
            modelBuilder.Entity<Deal>()
                .HasOne(bc => bc.Restaurant)
                .WithMany(b => b.Deals)
                .HasForeignKey(bc => bc.RestaurantId);

            modelBuilder.Entity<FavoriteDeal>()
                .HasKey(bc => new { bc.DealId, bc.UserId });

            modelBuilder.Entity<FavoriteDeal>()
                .HasOne(bc => bc.Deal)
                .WithMany(b => b.FavoriteDeals)
                .HasForeignKey(bc => bc.DealId);

            modelBuilder.Entity<FavoriteDeal>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.FavoriteDeals)
                .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<Purchase>()
                .HasKey(bc => new { bc.PurchaseId});

            modelBuilder.Entity<Purchase>()
                .HasOne(bc => bc.Deal)
                .WithMany(b => b.Purchases)
                .HasForeignKey(bc => bc.DealId);
            
            modelBuilder.Entity<Purchase>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.Purchases)
                .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<RefreshToken>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.RefreshTokens)
                .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<Restaurant>()
                .HasKey(bc => new { bc.RestaurantId });

            modelBuilder.Entity<Restaurant>()
                .HasIndex(bc => new { bc.Slug })
                .IsUnique();

            modelBuilder.Entity<Restaurant>()
                .HasOne(bc => bc.City)
                .WithMany(b => b.Restaurants)
                .HasForeignKey(bc => bc.CityId);

            modelBuilder.Entity<Restaurant>()
                .HasOne(bc => bc.Owner)
                .WithOne(b => b.Restaurant);
        }
    }
}
