﻿using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using System;

namespace explore_gidi_backend.Services.Implementations
{
    public class DealService : IDealService
    {
        private IDealRepository DealRepository { get; set; }
        private IEmailService EmailService { get; set; }
        private IPurchaseRepository PurchaseRepository { get; set; }

        public DealService(IDealRepository dealRepository,
                           IEmailService emailService,
                           IPurchaseRepository purchaseRepository)
        {
            DealRepository = dealRepository;
            EmailService = emailService;
            PurchaseRepository = purchaseRepository;
        }

        /// <summary>
        /// Inform partners of deals that expired the previous day
        /// </summary>
        public void CurateExpiredDeals()
        {
            // Curate deals that expired the previous day only
            // This is to avoid spamming the partner with a mail everyday after the deal expires
            // since this method will be called daily
            var expiredDeals = DealRepository.Get(x => x.ExpiryDate == 
                                                       DateTime.Today.AddDays(AppConstants.NEGATIVE_ONE),
                                                   includeProperties: "Restaurant.Owner");

            foreach (var deal in expiredDeals)
            {
                EmailService.SendExpiredDealEmail(deal.Restaurant.Owner.Email, 
                                                  deal.Restaurant.Name,
                                                  deal.Name, 
                                                  deal.ExpiryDate.ToString("MMMM dd, yyyy"));
            }
        }
        
        /// <summary>
        /// Remind users of purchased deals expiring in 48 hours or less
        /// </summary>
        public void CuratePurchasedDealsAboutToExpire()
        {
            var purchases = PurchaseRepository.Get(x => x.Deal.ExpiryDate <=
                                                        DateTime.Now.AddDays(AppConstants.TWO) &&
                                                        x.Deal.ExpiryDate >= DateTime.Now &&
                                                        !x.IsRedeemed,
                                                   includeProperties:"Deal,User");

            foreach (var purchase in purchases)
            {
                if (!(purchase.User.ShouldDelete || purchase.User.IsDeleted))
                {
                    var fullName = $"{purchase.User.FirstName} {purchase.User.LastName}".Trim();

                    EmailService.SendDealAboutToExpireEmail(purchase.User.Email,
                                                            fullName,
                                                            purchase.Deal.Name,
                                                            purchase.Deal.ExpiryDate.ToString("MMMM dd, yyyy"));
                }
            }
        }
    }
}
