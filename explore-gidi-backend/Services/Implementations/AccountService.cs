﻿using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;

namespace explore_gidi_backend.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private IEmailService EmailService { get; set; }
        private IUserRepository UserRepository { get; }

        public AccountService(IEmailService emailService,
                              IUserRepository userRepository)
        {
            EmailService = emailService;
            UserRepository = userRepository;
        }

        /// <summary>
        /// Flag user accounts that have been deactivated for over a week as deleted
        /// </summary>
        public void DeleteDeactivatedAccounts()
        {
            var users = UserRepository.Get(x => x.ShouldDelete &&
                                                x.TimeDeleteRequestReceived < 
                                                DateTime.Now.AddDays(AppConstants.NEGATIVE_SEVEN) &&
                                                !x.IsDeleted);

            foreach (var user in users)
            {
                UserRepository.FlagDeleted(user);

                try
                {
                    UserRepository.Update(user);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.TraceError("Error deleting a user\n" +
                                                        $"UserId: {user.Id} \n" +
                                                        $"StackTrace: {ex.StackTrace} \n" +
                                                        $"TimeStamp: {DateTime.Now}");
                    continue;
                }

                string userEmail = user.Email.Split('?', StringSplitOptions.None)
                                             .First();

                EmailService.SendDeletedAccountEmail(userEmail,
                    $"{user.FirstName} {user.LastName}".Trim());
            }
        }

        /// <summary>
        /// Reactivate a deactivated account
        /// </summary>
        /// <param name="user"></param>
        public void ReactivateAccount(ApplicationUser user)
        {
            UserRepository.ReActivateAccount(user);

            try
            {
                UserRepository.Update(user);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError("Error reactivating a user's account\n" +
                                                    $"UserId: {user.Id} \n" +
                                                    $"StackTrace: {ex.StackTrace} \n" +
                                                    $"TimeStamp: {DateTime.Now}");
            }

            EmailService.SendAccountReactivatedEmail(user.Email,
                $"{user.FirstName} {user.LastName}".Trim());
        }
    }
}
