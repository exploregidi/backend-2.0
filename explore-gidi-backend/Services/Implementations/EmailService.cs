using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace explore_gidi_backend.Services.Implementations
{
    public class EmailService : IEmailService
    {
        private SendGridClient sendGridClient;
        private string defaultFromEmail;
        private string defaultFromName;

        private IConfiguration Configuration { get; }

        public EmailService(IConfiguration configuration)
        {
            Configuration = configuration;

            sendGridClient = new SendGridClient(apiKey: Configuration["SendGridAPIKey"]);
            defaultFromEmail = Configuration["DefaultFromEmail"];
            defaultFromName = Configuration["DefaultFromEmailName"];
        }

        /// <summary>
        /// Send an email using SendGrid
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="toFullname"></param>
        /// <param name="subject"></param>
        /// <param name="emailTemplate"></param>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        private void SendEmail(string toEmail,
                               string toFullname,
                               string subject,
                               EmailTemplate emailTemplate,
                               Dictionary<EmailReplacementKey, string> dictionary)
        {
            string emailBody = string.Empty;

            using (StreamReader streamReader = new StreamReader($"{AppContext.BaseDirectory}/EmailTemplates/{emailTemplate.filename}"))
            {
                emailBody = streamReader.ReadToEnd();
            }

            foreach (KeyValuePair<EmailReplacementKey, string> pair in dictionary)
            {
                emailBody = emailBody.Replace(pair.Key.KeyString, pair.Value);
            }

            var msg = new SendGridMessage()
            {
                From = new EmailAddress(defaultFromEmail, defaultFromName),
                Subject = subject,
                HtmlContent = emailBody
            };
            msg.AddTo(new EmailAddress(toEmail, toFullname));
            var response = sendGridClient.SendEmailAsync(msg).Result;

            if (response.StatusCode != HttpStatusCode.OK)
            {
                System.Diagnostics.Trace.TraceError($"{emailTemplate.filename} to {toEmail} " +
                                                    $"had a status of {response.StatusCode}" +
                                                    $"TimeStamp: {DateTime.Now}");
            }
        }

        /// <summary>
        /// User has dactivated their account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendAccountDeactivatedEmail(string email, string fullname)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Account Deactivated", EmailTemplate.ACCOUNT_DEACTIVATED,
                replacementKeys);
        }

        /// <summary>
        /// User has reactivated their account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendAccountReactivatedEmail(string email, string fullname)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Account Reactivated", EmailTemplate.ACCOUNT_REACTIVATED,
                replacementKeys);
        }

        /// <summary>
        /// User requested to confirm their email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        /// <param name="confirmEmailToken"></param>
        public void SendConfirmEmail(string email, string fullname, string confirmEmailToken)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname },
                {EmailReplacementKey.TIME_ONE_TIME_PASSWORD, confirmEmailToken },
            };
            SendEmail(email, fullname, "Confirm your Gidi Eats email", EmailTemplate.CONFIRM_EMAIL,
                replacementKeys);
        }

        /// <summary>
        /// Purchased deal is about to expire
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        /// <param name="dealName"></param>
        /// <param name="dealExpiryDate"></param>
        public void SendDealAboutToExpireEmail(string email, string fullname, string dealName, string dealExpiryDate)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.DEAL_NAME, dealName },
                {EmailReplacementKey.DEAL_EXPIRY_DATE, dealExpiryDate },
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Your purchased deal is about to expire", EmailTemplate.DEAL_ABOUT_TO_EXPIRE,
                replacementKeys);
        }

        /// <summary>
        /// Send email for a deleted account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendDeletedAccountEmail(string email, string fullname)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Goodbye from Gidi Eats", EmailTemplate.DELETE_ACCOUNT_SUCCESSFUL,
                replacementKeys);
        }

        /// <summary>
        /// A deal has expired
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        /// <param name="dealName"></param>
        /// <param name="dealExpiryDate"></param>
        public void SendExpiredDealEmail(string email, string fullname, string dealName, string dealExpiryDate)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.DEAL_NAME, dealName },
                {EmailReplacementKey.DEAL_EXPIRY_DATE, dealExpiryDate },
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Your deal has expired", EmailTemplate.EXPIRED_DEAL,
                replacementKeys);
        }

        /// <summary>
        /// User requested to reset their password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="passwordResetToken"></param>
        /// <param name="fullname"></param>
        public void SendForgotPasswordEmail(string email, string fullname, string passwordResetToken)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname },
                {EmailReplacementKey.TIME_ONE_TIME_PASSWORD, passwordResetToken },
            };
            SendEmail(email, fullname, "Reset your Gidi Eats password", EmailTemplate.FORGOT_PASSWORD,
                replacementKeys);
        }

        /// <summary>
        /// User has changed their password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendPasswordChangedEmail(string email, string fullname)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Your Gidi Eats password has changed", EmailTemplate.PASSWORD_CHANGED_ALERT,
                replacementKeys);
        }

        /// <summary>
        /// User has successfully reset their password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendPasswordResetEmail(string email, string fullname)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname },
            };
            SendEmail(email, fullname, "Your Gidi Eats password reset was successful", EmailTemplate.PASSWORD_RESET_SUCCESSFUL,
                replacementKeys);
        }

        /// <summary>
        /// Send an email on successful registration
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendSuccessfulRegistrationMessage(string email, string fullname)
        {
            var replacementKeys = new Dictionary<EmailReplacementKey, string>
            {
                {EmailReplacementKey.USERNAME, fullname }
            };
            SendEmail(email, fullname, "Welcome to Gidi Eats", EmailTemplate.REGISTRATION_SUCCESSFUL,
                replacementKeys);
        }
    }
}
