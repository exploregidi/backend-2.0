﻿using Cronos;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace explore_gidi_backend.Services.Implementations
{
    public abstract class CronJobService : IHostedService, IDisposable
    {
        private System.Timers.Timer Timer;
        private readonly CronExpression Expression;
        private readonly TimeZoneInfo TimeZoneInfo;

        protected CronJobService(string cronExpression, TimeZoneInfo timeZoneInfo)
        {
            Expression = CronExpression.Parse(cronExpression);
            TimeZoneInfo = timeZoneInfo;
        }

        public virtual async Task StartAsync(CancellationToken cancellationToken)
        {
            await ScheduleJob(cancellationToken);
        }

        protected virtual async Task ScheduleJob(CancellationToken cancellationToken)
        {
            var next = Expression.GetNextOccurrence(DateTimeOffset.Now, TimeZoneInfo);

            if (next.HasValue)
            {
                var delay = next.Value - DateTimeOffset.Now;
                Timer = new System.Timers.Timer(delay.TotalMilliseconds);

                Timer.Elapsed += async (sender, args) =>
                {
                    Timer.Dispose();  // reset and dispose timer
                    Timer = null;

                    if (!cancellationToken.IsCancellationRequested)
                    {
                        await DoWork(cancellationToken);
                    }

                    if (!cancellationToken.IsCancellationRequested)
                    {
                        await ScheduleJob(cancellationToken);    // reschedule next
                    }
                };

                Timer.Start();
            }

            await Task.CompletedTask;
        }

        public virtual async Task DoWork(CancellationToken cancellationToken)
        {
            await Task.Delay(5000, cancellationToken);  // do the work
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            Timer?.Stop();
            await Task.CompletedTask;
        }

        public virtual void Dispose()
        {
            Timer?.Dispose();
        }
    }
}
