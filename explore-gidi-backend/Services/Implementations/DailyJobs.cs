﻿using explore_gidi_backend.Services.Interfaces;
using explore_gidi_backend.Utilities;
using System.Threading;
using System.Threading.Tasks;

namespace explore_gidi_backend.Services.Implementations
{
    /// <summary>
    /// Tasks scheduled to run daily
    /// </summary>
    public class DailyJobs : CronJobService
    {
        private IAccountService AccountService { get; set; }
        private IDealService DealService { get; set; }

        /// <summary>
        /// Tasks scheduled to run daily
        /// </summary>
        /// <param name="accountService"></param>
        /// <param name="dealService"></param>
        /// <param name="config"></param>
        public DailyJobs(IAccountService accountService,
                         IDealService dealService,
                         IScheduleConfig<DailyJobs> config) : 
            base(config.CronExpression, config.TimeZoneInfo)
        {
            AccountService = accountService;
            DealService = dealService;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }

        /// <summary>
        /// Actual task to perform
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task DoWork(CancellationToken cancellationToken)
        {
            DealService.CuratePurchasedDealsAboutToExpire();
            AccountService.DeleteDeactivatedAccounts();
            DealService.CurateExpiredDeals();

            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        }
    }
}
