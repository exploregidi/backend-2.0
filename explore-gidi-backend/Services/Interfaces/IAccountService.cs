﻿using explore_gidi_backend.Models;

namespace explore_gidi_backend.Services.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Flag user accounts that have been deactivated for over a week as deleted
        /// </summary>
        public void DeleteDeactivatedAccounts();

        /// <summary>
        /// Reactivate a deactivated account
        /// </summary>
        /// <param name="user"></param>
        public void ReactivateAccount(ApplicationUser user);
    }
}
