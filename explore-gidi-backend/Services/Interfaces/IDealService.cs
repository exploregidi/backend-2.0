﻿namespace explore_gidi_backend.Services.Interfaces
{
    public interface IDealService
    {
        /// <summary>
        /// Inform partners of deals that expired the previous day
        /// </summary>
        public void CurateExpiredDeals();

        /// <summary>
        /// Remind users of purchased deals expiring in 48 hours or less
        /// </summary>
        public void CuratePurchasedDealsAboutToExpire();
    }
}
