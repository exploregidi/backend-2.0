﻿namespace explore_gidi_backend.Services.Interfaces
{
    public interface IEmailService
    {
        /// <summary>
        /// User has dactivated their account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendAccountDeactivatedEmail(string email, string fullname);

        /// <summary>
        /// User has reactivated their account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendAccountReactivatedEmail(string email, string fullname);

        /// <summary>
        /// User requested to confirm their email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="confirmEmailToken"></param>
        /// <param name="fullname"></param>
        public void SendConfirmEmail(string email, string fullname, string confirmEmailToken);

        /// <summary>
        /// Purchased deal is about to expire
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        /// <param name="dealName"></param>
        /// <param name="dealExpiryDate"></param>
        public void SendDealAboutToExpireEmail(string email, string fullname, string dealName, string dealExpiryDate);

        /// <summary>
        /// Send email for a deleted account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendDeletedAccountEmail(string email, string fullname);

        /// <summary>
        /// A deal has expired
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        /// <param name="dealName"></param>
        /// <param name="dealExpiryDate"></param>
        public void SendExpiredDealEmail(string email, string fullname, string dealName, string dealExpiryDate);

        /// <summary>
        /// User requested to reset their password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="passwordResetToken"></param>
        /// <param name="fullname"></param>
        public void SendForgotPasswordEmail(string email, string fullname, string passwordResetToken);

        /// <summary>
        /// User has changed their password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendPasswordChangedEmail(string email, string fullname);

        /// <summary>
        /// User has successfully reset their password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendPasswordResetEmail(string email, string fullname);

        /// <summary>
        /// Send an email on successful registration
        /// </summary>
        /// <param name="email"></param>
        /// <param name="fullname"></param>
        public void SendSuccessfulRegistrationMessage(string email, string fullname);
    }
}
