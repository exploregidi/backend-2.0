﻿using Microsoft.AspNetCore.Identity;
using System;

namespace explore_gidi_backend.Repositories.Interfaces
{
    public interface IUserLoginRepository : IGenericRepository<IdentityUserLogin<Guid>>
    {
    }
}
