﻿using explore_gidi_backend.Models;

namespace explore_gidi_backend.Repositories.Interfaces
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
