﻿using explore_gidi_backend.Models;

namespace explore_gidi_backend.Repositories.Interfaces
{
    public interface IDealRepository : IGenericRepository<Deal>
    {
        /// <summary>
        /// Generates the slug for a deal
        /// </summary>
        /// <param name="deal"></param>
        void ConfigureDetails(Deal deal);

        /// <summary>
        /// Flags a deal as deleted
        /// </summary>
        /// <param name="deal"></param>
        public void FlagDeletedDeal(Deal deal);
    }
}