﻿using explore_gidi_backend.Models;

namespace explore_gidi_backend.Repositories.Interfaces
{
    public interface IUserRepository : IGenericRepository<ApplicationUser>
    {
        /// <summary>
        /// Set details for a user account's reactivation
        /// </summary>
        /// <param name="user"></param>
        public void ReActivateAccount(ApplicationUser user);

        /// <summary>
        /// Flag a user as deleted
        /// </summary>
        /// <param name="user"></param>
        public void FlagDeleted(ApplicationUser user);

        /// <summary>
        /// Flags a user for deleting
        /// </summary>
        /// <param name="user"></param>
        void FlagForDeletion(ApplicationUser user);
    }
}
