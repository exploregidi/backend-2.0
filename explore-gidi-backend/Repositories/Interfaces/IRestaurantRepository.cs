﻿using explore_gidi_backend.Models;

namespace explore_gidi_backend.Repositories.Interfaces
{
    public interface IRestaurantRepository : IGenericRepository<Restaurant>
    {
        /// <summary>
        /// Configure the Slug of a restaurant
        /// </summary>
        /// <param name="restaurant"></param>
        public void ConfigureDetails(Restaurant restaurant);
    }
}
