﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;
using System;
using System.Linq;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(MariaDbContext context) : base(context, context.ApplicationUsers) { }


        public void ReActivateAccount(ApplicationUser user)
        {
            user.ShouldDelete = false;
            user.TimeReactivated = DateTime.Now;
        }

        public void FlagDeleted(ApplicationUser user)
        {
            user.Email = ScrambleEmail(user);
            user.NormalizedEmail = user.Email.ToUpper();
            user.PasswordHash = string.Join("", user.PasswordHash.Reverse());
            user.UserName = user.Email;
            user.NormalizedUserName = user.UserName.ToUpper();
            user.IsDeleted = true;
            user.TimeDeleted = DateTime.Now;
        }

        public void FlagForDeletion(ApplicationUser user)
        {
            user.ShouldDelete = true;
            user.TimeDeleteRequestReceived = DateTime.Now;
        }

        /// <summary>
        /// Scramble a user's email address
        /// </summary>
        /// <param name="user"></param>
        string ScrambleEmail(ApplicationUser user)
        {
            string scrambledEmail = user.Email + "?deleted";
            string[] tokenizedId = user.Id.ToString().Split("-");
            var index = 0;

            while (Get(x => x.Email == scrambledEmail).FirstOrDefault() != null)
            {
                scrambledEmail += "." + tokenizedId[index];
                index++;

                if (index >= tokenizedId.Length)
                {
                    scrambledEmail = user.Email + "?deleted." + user.Id.ToString().Replace('-', '.');
                    break;
                }
            }

            return scrambledEmail;
        }
    }
}