﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class UserLoginRepository : GenericRepository<IdentityUserLogin<Guid>>, IUserLoginRepository
    {
        public UserLoginRepository(MariaDbContext context) : base(context, context.UserLogins) { }
    }
}
