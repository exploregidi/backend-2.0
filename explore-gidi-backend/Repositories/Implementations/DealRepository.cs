﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using System.Linq;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class DealRepository : GenericRepository<Deal>, IDealRepository
    {
        public DealRepository(MariaDbContext context) : base(context, context.Deals) {}

        /// <summary>
        /// Generates the slug for a deal
        /// </summary>
        /// <param name="deal"></param>
        public void ConfigureDetails(Deal deal)
        {
            deal.Slug = GenerateSlug(deal);
        }

        /// <summary>
        /// Flags a deal as deleted
        /// </summary>
        /// <param name="deal"></param>
        public void FlagDeletedDeal(Deal deal)
        {
            deal.IsDeleted = true;
        }

        /// <summary>
        /// Generate a Slug for a deal.
        /// </summary>
        /// <param name="deal"></param>
        string GenerateSlug(Deal deal)
        {
            string slug = RegexUtilities.ToUrlSlug(deal.Name);
            string[] tokenizedId = deal.DealId.ToString().Split("-");
            var index = 0;

            while (Get(x => x.Slug == slug).FirstOrDefault() != null)
            {
                slug += "-" + tokenizedId[index];
                index++;

                if (index >= tokenizedId.Length)
                {
                    slug = RegexUtilities.ToUrlSlug(deal.Name) + "-" + deal.DealId;
                    break;
                }
            }

            return slug;
        }
    }
}
