﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class PurchaseRepository : GenericRepository<Purchase>, IPurchaseRepository
    {
        public PurchaseRepository(MariaDbContext context) : base(context, context.Purchases) { }
    }
}
