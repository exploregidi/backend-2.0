﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using System.Linq;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class RestaurantRepository : GenericRepository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(MariaDbContext context) : base(context, context.Restaurants) { }

        public void ConfigureDetails(Restaurant restaurant)
        {
            restaurant.Slug = GenerateSlug(restaurant);
        }

        /// <summary>
        /// Generate a Slug for a Restaurant.
        /// </summary>
        /// <param name="restaurant"></param>
        string GenerateSlug(Restaurant restaurant)
        {
            string slug = RegexUtilities.ToUrlSlug(restaurant.Name);
            string[] tokenizedId = restaurant.RestaurantId.ToString().Split("-");
            var index = 0;

            while (Get(x => x.Slug == slug).FirstOrDefault() != null)
            {
                slug += "-" + tokenizedId[index];
                index++;

                if (index >= tokenizedId.Length)
                {
                    slug = RegexUtilities.ToUrlSlug(restaurant.Name) + "-" + restaurant.RestaurantId;
                    break;
                }
            }

            return slug;
        }
    }
}
