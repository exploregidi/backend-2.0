﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class FavoriteDealRepository : GenericRepository<FavoriteDeal>, IFavoriteDealRepository
    {
        public FavoriteDealRepository(MariaDbContext context) : base(context, context.FavoriteDeals) { }
    }
}
