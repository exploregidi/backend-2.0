﻿using explore_gidi_backend.DBContexts;
using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;

namespace explore_gidi_backend.Repositories.Implementations
{
    public class RefreshTokenRepository : GenericRepository<RefreshToken>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(MariaDbContext context) : base(context, context.RefreshTokens) { }
    }
}
