﻿using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace explore_gidi_backend.DBInitializers
{
    public class DummyDataInitializer
    {
        public static void SeedDummyData(ICategoryRepository categoryRepository,
                                         ICityRepository cityRepository,
                                         IConfiguration configuration,
                                         IDealRepository dealRepository,
                                         IRestaurantRepository restaurantRepository,
                                         UserManager<ApplicationUser> userManager)
        {
            SeedRestaurants(cityRepository, configuration, restaurantRepository, userManager);
            SeedDeals(categoryRepository, dealRepository, restaurantRepository);
        }

        private static void SeedRestaurants(ICityRepository cityRepository,
                                            IConfiguration configuration,
                                            IRestaurantRepository restaurantRepository,
                                            UserManager<ApplicationUser> userManager)
        {
            if (restaurantRepository.Get().Count() > 20)
                return;

            string defaultPartnerEmail = configuration["DefaultPartnerEmail"];
            var defaultPartner = userManager.FindByEmailAsync(defaultPartnerEmail).Result;

            var cities = cityRepository.Get();
            
            foreach (var city in cities)
            {
                int totalRestaurants = new Random().Next(5, 10);

                for (int i = 1; i <= totalRestaurants; i++)
                {
                    var restaurant = new Restaurant
                    {
                        Name = $"{city.Name} Restaurant {i.ToString("D3")}",
                        Slug = RegexUtilities.ToUrlSlug($"{city.Name} Restaurant {i.ToString("D3")}"),
                        ImageUrl = AppConstants.CAFE_BAR_IMAGE_URL,
                        CityId = city.CityId
                    };

                    if (restaurantRepository.Get(x => x.Name == restaurant.Name).Count() == 0)
                        restaurantRepository.Insert(restaurant);
                }
            }
        }

        private static void SeedDeals(ICategoryRepository categoryRepository,
                                      IDealRepository dealRepository,
                                      IRestaurantRepository restaurantRepository)
        {
            if (dealRepository.Get().Count() > 100)
                return;

            var restaurants = restaurantRepository.Get();

            var featuredCategory = categoryRepository.Get(x => x.Name == AppConstants.FEATURED)
                                                     .FirstOrDefault();

            foreach (var restaurant in restaurants)
            {
                int totalDeals = new Random().Next(5, 10);

                for (int i = 1; i <= totalDeals; i++)
                {
                    var deal = new Deal
                    {
                        Name = $"{restaurant.Name} Deal {i.ToString("D3")}",
                        Slug = RegexUtilities.ToUrlSlug($"{restaurant.Name} Deal {i.ToString("D3")}"),
                        Description = $"This is the description for the deal '{restaurant.Name} Deal {i.ToString("D3")}' that belongs to {restaurant.Name}",
                        DiscountPrice = AppConstants.THREE_THOUSAND_AND_TEN,
                        ValuePrice = AppConstants.SIX_THOUSAND_DOT_FIVE_FIVE,
                        PercentOff = (double)(AppConstants.THREE_THOUSAND_AND_TEN / AppConstants.SIX_THOUSAND_DOT_FIVE_FIVE * AppConstants.ONE_HUNDRED),
                        ExpiryDate = DateTime.Now.AddDays(i),
                        ImageUrl = AppConstants.MOON_PICTURE_URL,
                        CategoryId = featuredCategory.CategoryId,
                        RestaurantId = restaurant.RestaurantId
                    };

                    if (dealRepository.Get(x => x.Name == deal.Name).Count() == 0)
                        dealRepository.Insert(deal);
                }
            }
        }
    }
}
