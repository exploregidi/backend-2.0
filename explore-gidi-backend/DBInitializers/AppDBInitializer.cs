﻿using explore_gidi_backend.Models;
using explore_gidi_backend.Repositories.Interfaces;
using explore_gidi_backend.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace explore_gidi_backend.DBInitializers
{
    public class AppDbInitializer
    {
        public static void SeedDatabase(IConfiguration configuration,
                                        ILogger<AppDbInitializer> logger,
                                        ICategoryRepository categoryRepository,
                                        ICityRepository cityRepository,
                                        IDealRepository dealRepository,
                                        IRestaurantRepository restaurantRepository,
                                        RoleManager<ApplicationRole> roleManager,
                                        UserManager<ApplicationUser> userManager)
        {
            SeedRolesAndUsers(configuration, logger, roleManager, userManager);
            SeedCategories(categoryRepository);
            SeedCities(cityRepository);

            string seedDummyItems = configuration["SeedDummyItems"];
            if (bool.TryParse(seedDummyItems, out _))
            {
                DummyDataInitializer.SeedDummyData(categoryRepository,
                                                   cityRepository,
                                                   configuration,
                                                   dealRepository,
                                                   restaurantRepository,
                                                   userManager);
            }
        }

        private static void SeedRolesAndUsers(IConfiguration configuration,
                                              ILogger<AppDbInitializer> logger,
                                              RoleManager<ApplicationRole> roleManager,
                                              UserManager<ApplicationUser> userManager)
        {
            if (roleManager.FindByNameAsync(UserRoleConstants.ADMIN).Result == null)
            {
                var result = roleManager.CreateAsync(new ApplicationRole(UserRoleConstants.ADMIN)).Result;

                if (result.Succeeded)
                {
                    System.Diagnostics.Trace.TraceInformation("Created the Admin Role\n" +
                        $"TimeStamp: {DateTime.Now}");
                }
                else
                {
                    System.Diagnostics.Trace.TraceError("There was an error creating the Admin role\n" +
                        $"Errors: {result.Errors} \n" +
                        $"TimeStamp: {DateTime.Now}");
                }
            }

            if (roleManager.FindByNameAsync(UserRoleConstants.PARTNER).Result == null)
            {
                var result = roleManager.CreateAsync(new ApplicationRole(UserRoleConstants.PARTNER)).Result;

                if (result.Succeeded)
                {
                    System.Diagnostics.Trace.TraceInformation("Created the Partner Role\n" +
                        $"TimeStamp: {DateTime.Now}");
                }
                else
                {
                    System.Diagnostics.Trace.TraceError("There was an error creating the Partner role\n" +
                        $"Errors: {result.Errors} \n" +
                        $"TimeStamp: {DateTime.Now}");
                }
            }

            if (roleManager.FindByNameAsync(UserRoleConstants.SUPER_ADMIN).Result == null)
            {
                var result = roleManager.CreateAsync(new ApplicationRole(UserRoleConstants.SUPER_ADMIN)).Result;

                if (result.Succeeded)
                {
                    System.Diagnostics.Trace.TraceInformation("Created the Super Admin Role\n" +
                        $"TimeStamp: {DateTime.Now}");
                }
                else
                {
                    System.Diagnostics.Trace.TraceError("There was an error creating the Super Admin role\n" +
                        $"Errors: {result.Errors} \n" +
                        $"TimeStamp: {DateTime.Now}");
                }
            }

            if (roleManager.FindByNameAsync(UserRoleConstants.USER).Result == null)
            {
                var result = roleManager.CreateAsync(new ApplicationRole(UserRoleConstants.USER)).Result;

                if (result.Succeeded)
                {
                    System.Diagnostics.Trace.TraceInformation("Created the User Role\n" +
                        $"TimeStamp: {DateTime.Now}");
                }
                else
                {
                    System.Diagnostics.Trace.TraceError("There was an error creating the User role\n" +
                        $"Errors: {result.Errors} \n" +
                        $"TimeStamp: {DateTime.Now}");
                }
            }

            string defaultPartnerEmail = configuration["DefaultPartnerEmail"];
            string defaultPartnerPhone = configuration["DefaultPartnerPhone"];
            string defaultPartnerPassword = configuration["DefaultPartnerPassword"];

            // Setup default partner user only if it is setup in appsettings
            if (!string.IsNullOrEmpty(defaultPartnerEmail) && !string.IsNullOrEmpty(defaultPartnerPassword))
            {
                if (userManager.FindByEmailAsync(defaultPartnerEmail).Result == null)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        UserName = defaultPartnerEmail,
                        NormalizedUserName = defaultPartnerEmail.ToUpper(),
                        Email = defaultPartnerEmail,
                        NormalizedEmail = defaultPartnerEmail.ToUpper(),
                        EmailConfirmed = true,
                        PhoneNumber = defaultPartnerPhone,
                        PhoneNumberConfirmed = true,
                        FirstName = "Default",
                        LastName = "Partner",
                        ProfilePictureUrl = AppConstants.MOON_PICTURE_URL
                    };

                    try
                    {
                        IdentityResult result = userManager.CreateAsync(user, defaultPartnerPassword).Result;

                        if (result.Succeeded)
                        {
                            userManager.AddToRoleAsync(user, UserRoleConstants.PARTNER).Wait();
                            userManager.AddToRoleAsync(user, UserRoleConstants.USER).Wait();

                            System.Diagnostics.Trace.TraceInformation("Created the Default Partner\n" +
                                $"TimeStamp: {DateTime.Now}");
                        }
                        else
                        {
                            System.Diagnostics.Trace.TraceError("There was an error creating the Default Partner\n" +
                                $"Errors: {result.Errors} \n" +
                                $"TimeStamp: {DateTime.Now}");
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError("There was an error creating the Default Partner\n" +
                                $"StackTrace: {ex.StackTrace} \n" +
                                $"TimeStamp: {DateTime.Now}");
                    }
                }
            }

            string superAdminEmail = configuration["SuperAdminEmail"];
            string superAdminPassword = configuration["SuperAdminPassword"];

            // Setup super admin user only if it is setup in appsettings
            if (!string.IsNullOrEmpty(superAdminEmail) && !string.IsNullOrEmpty(superAdminPassword))
            {
                if (userManager.FindByEmailAsync(superAdminEmail).Result == null)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        UserName = superAdminEmail,
                        NormalizedUserName = superAdminEmail.ToUpper(),
                        Email = superAdminEmail,
                        NormalizedEmail = superAdminEmail.ToUpper(),
                        EmailConfirmed = true,
                        FirstName = "Super",
                        LastName = "Admin"
                    };

                    try
                    {
                        IdentityResult result = userManager.CreateAsync(user, superAdminPassword).Result;

                        if (result.Succeeded)
                        {
                            userManager.AddToRoleAsync(user, UserRoleConstants.SUPER_ADMIN).Wait();
                            userManager.AddToRoleAsync(user, UserRoleConstants.USER).Wait();

                            System.Diagnostics.Trace.TraceInformation("Created the Super Admin User\n" +
                                $"TimeStamp: {DateTime.Now}");
                        }
                        else
                        {
                            System.Diagnostics.Trace.TraceError("There was an error creating the Super Admin User\n" +
                                $"Errors: {result.Errors} \n" +
                                $"TimeStamp: {DateTime.Now}");
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.TraceError("There was an error creating the Super Admin User\n" +
                                $"StackTrace: {ex.StackTrace} \n" +
                                $"TimeStamp: {DateTime.Now}");
                    }
                }
            }
        }

        private static void SeedCategories(ICategoryRepository categoryRepository)
        {
            if (categoryRepository.Get().Count() > 0)
                return;

            var categories = Content.Categories();

            foreach (var category in categories)
            {
                if (categoryRepository.Get(x => x.Name == category.Name)
                                      .Count() == 0)
                {
                    categoryRepository.Insert(category);
                }
            }
        }

        private static void SeedCities(ICityRepository cityRepository)
        {
            if (cityRepository.Get().Count() > 0)
                return;

            var cities = Content.Cities();

            foreach (var city in cities)
            {
                if (cityRepository.Get(x => x.Name == city.Name)
                                  .Count() == 0)
                {
                    cityRepository.Insert(city);
                }
            }
        }
    }
}
